/*
 * java-access-bridge for GNOME
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Accessibility;
import org.GNOME.Bonobo.*;
import org.GNOME.Accessibility.AccessUtil;

public class ListenerUtil {

	public static class KeyListener extends DeviceEventListenerImpl {

		private AccessUtil.EventCallback callback;
		private DeviceEventListener objRef = null;

		public KeyListener () {
			super ();
		}

		public KeyListener (AccessUtil.EventCallback callback) {
			this ();
			this.callback = callback;
		}

		public DeviceEventListener objRef () {
			if (objRef == null)
				objRef = DeviceEventListenerHelper.narrow (
					this.tie ());
			System.out.println ("returning objref");
			return objRef;
		}

		public boolean notifyEvent (DeviceEvent e) {
			System.out.println ("notify");
			return (callback != null) && callback.notifyDeviceEvent (e);
		}
	        
	        public void unImplemented__ () {}
	        public void unImplemented_2_ () {}
	        public void unImplemented_3_ () {}
	        public void unImplemented_4_ () {}
	        public void unImplemented_5_ () {}
	        public void unImplemented_6_ () {}
	};
}
