/*
 * java-access-bridge for GNOME
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

package org.GNOME.Accessibility;
import org.GNOME.Bonobo.*;
import org.omg.PortableServer.*;
import org.omg.CORBA.*;
import java.io.*;
import java.util.*;


public class EventQueueRegistry extends org.GNOME.Accessibility._RegistryStub {
  
  Registry              registry;
  EventProcessor        eventProcessor;
  Thread                eventProcessorThread;

  public EventQueueRegistry( Registry reg ) {
        if ( reg != null ) {
            registry = reg;
            registry.ref();
            this._set_delegate( ((org.omg.CORBA.portable.ObjectImpl)reg)._get_delegate () );
            
            eventProcessor = new EventProcessor();
            eventProcessorThread = new Thread( eventProcessor );
            eventProcessorThread.start();
        }
        else {
            System.err.println("Unable to contact registry");
            registry = null;
            eventProcessor = null;
            eventProcessorThread = null;
            return;
        }
  }

  /*
   * Registry
   */
    
  public void registerApplication (org.GNOME.Accessibility.Application application) {
        if ( registry != null ) {
            registry.registerApplication( application );
        }
  }

  public void deregisterApplication (org.GNOME.Accessibility.Application application) {
        if ( registry != null ) {
            registry.deregisterApplication( application );
        }
  }

  public void registerGlobalEventListener (org.GNOME.Accessibility.EventListener listener, 
                                    String eventName) {
        if ( registry != null ) {
            registry.registerGlobalEventListener( listener, eventName );
        }
  }

  public void deregisterGlobalEventListenerAll (org.GNOME.Accessibility.EventListener listener) {
        if ( registry != null ) {
            registry.deregisterGlobalEventListenerAll (listener);
        }
  }

  public void deregisterGlobalEventListener (org.GNOME.Accessibility.EventListener listener,
                                      String event_name) {
        if ( registry != null ) {
            registry.deregisterGlobalEventListener (listener, event_name);
        }
  }

  public short getDesktopCount () {
        if ( registry != null ) {
            return registry.getDesktopCount ();
        }
        else {
            return (short)0;
        }
  }

  public org.GNOME.Accessibility.Desktop getDesktop (short n) {
        if ( registry != null ) {
            return registry.getDesktop (n);
        }
        else {
            return null;
        }
  }

  public org.GNOME.Accessibility.Desktop[] getDesktopList () {
        if ( registry != null ) {
            return registry.getDesktopList ();
        }
        else {
            return null;
        }
  }

  public org.GNOME.Accessibility.DeviceEventController getDeviceEventController () {
        if ( registry != null ) {
            return registry.getDeviceEventController ();
        }
        else {
            return null;
        }
  }

  public void unImplemented () {
        if ( registry != null ) {
            registry.unImplemented();
        }
  }

  public void unImplemented2 () {
        if ( registry != null ) {
            registry.unImplemented2();
        }
  }

  /*
   * EventListener
   */
  public void notifyEvent (org.GNOME.Accessibility.Event e) {
        if ( eventProcessor != null ) {
            eventProcessor.postEvent( e );
        }
  }

  /*
   * Unknown
   */
  public void ref () {
        if ( registry != null ) {
            registry.ref();
        }
  }

  public void unref () {
        if ( registry != null ) {
            registry.unref();
        }
  }

  public org.GNOME.Bonobo.Unknown queryInterface (String repoid) {
        if ( registry != null ) {
            return registry.queryInterface (repoid);
        }
        else {
            return null;
        }
  }

  class EventProcessor implements Runnable {
     LinkedList eventList;

     EventProcessor() {
        eventList = new LinkedList();
     }
 
     public void run() {
        while ( true ) {
            while ( numEvents() == 0 ) {
                try {
                    synchronized( this ) {
                        wait(); 
                    }
                }
                catch ( InterruptedException e ) {}
            }
            processEvent();
        }
     }

     public int numEvents() {
        int size;
        synchronized (eventList) {
            size = eventList.size();
        }
        return size;
     }

     public void postEvent( org.GNOME.Accessibility.Event e ) {
        // System.err.println("postEvent: " +e.type );
        synchronized (eventList) {
            eventList.addLast( e );
        }
        synchronized( this ) {
            notify();
        }
     }

     public void processEvent( ) {
        org.GNOME.Accessibility.Event e;
        try {
            synchronized (eventList) {
                e = (org.GNOME.Accessibility.Event)eventList.removeFirst();
            }
            // System.err.println("processEvent: " +e.type );
            if ( registry != null ) {
                registry.notifyEvent( e );
            }
        }
        catch ( Exception ex ) {
			ex.printStackTrace();
            System.err.println("processEvent : Exception Caught : " + ex.toString() );
        }
     }
  }

} 
