/*
 * java-access-bridge for GNOME
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Accessibility;

public class KeyMask {

	public static KeyMask UNMODIFIED;
	public static KeyMask LOCK;
	public static KeyMask SHIFT;
	public static KeyMask CONTROL;
	public static KeyMask MOD1;
	public static KeyMask MOD2;
	public static KeyMask MOD3;
	public static KeyMask MOD4;
	public static KeyMask MOD5;
	public static KeyMask NUMLOCK;
	public static KeyMask ALT;

	static final int _UNMODIFIED = 0;
	static final int _SHIFT =   (1<<0);
	static final int _LOCK =    (1<<1);
	static final int _CONTROL = (1<<2);
	static final int _MOD1 = (1<<3);
	static final int _MOD2 = (1<<4);
	static final int _MOD3 = (1<<5);
	static final int _MOD4 = (1<<6);
	static final int _MOD5 = (1<<7);
	static final int _NUMLOCK = (1<<14);
	static final int _ALT =  _MOD1;

	static {
		SHIFT =    new KeyMask (_SHIFT);
		LOCK =     new KeyMask (_LOCK);
		CONTROL =  new KeyMask (_CONTROL);
		MOD1 =     new KeyMask (_MOD1);
		MOD2 =     new KeyMask (_MOD2);
		MOD3 =     new KeyMask (_MOD3);
		MOD4 =     new KeyMask (_MOD4);
		MOD5 =     new KeyMask (_MOD5);
		MOD5 =     new KeyMask (_MOD5);
		NUMLOCK =  new KeyMask (_NUMLOCK);
		ALT =      MOD1;
	}

	private int val;

	public KeyMask (int i) {
		this.val = i;
	}

	public int value () {
		return val;
	}

	public String toString () {
		switch (val) {
		case _UNMODIFIED:
			return "";
		case _SHIFT:
			return "Shift";
		case _LOCK:
			return "CapsLock";
		case _CONTROL:
			return "Control";
		case _ALT:
			return "Alt";
		case _MOD2:
			return "Mod2";
		case _MOD3:
			return "Mod3";
		case _MOD4:
			return "Mod4";
		case _MOD5:
			return "Mod5";
		case _NUMLOCK:
			return "NumLock";
		default:
			return "?";
		}
	}
}
