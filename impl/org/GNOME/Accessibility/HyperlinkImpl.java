/*
 * java-access-bridge for GNOME
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Accessibility;
import javax.accessibility.*;
import org.GNOME.Bonobo.*;

public class HyperlinkImpl extends UnknownImpl implements HyperlinkOperations {

	AccessibleHyperlink hl;

	public HyperlinkImpl ( AccessibleHyperlink hl ) {
		super();
		this.hl = hl;
		this.poa = JavaBridge.getRootPOA();
                tie = new HyperlinkPOATie (this, JavaBridge.getRootPOA());
		this.aggregate_interfaces ();
                try {
                        JavaBridge.getRootPOA().activate_object(tie);
                } catch (Exception e) {
                        System.out.println (e);
                }
	}

	private void aggregate_interfaces () {
		if (hl != null) {
			UnknownImpl iface;

			iface = new ActionImpl (hl);
			this.add_interface (iface);
		}
	}

	public short nAnchors () {
		short n = 0;
		if (hl != null)
			n = (short) hl.getAccessibleActionCount();
		return n;
	}

	public int startIndex () {
		return hl.getStartIndex();
	}
	
	public int endIndex () {
		return hl.getEndIndex();
	}
	
	public org.GNOME.Accessibility.Accessible getObject (int i) {
		org.GNOME.Accessibility.Accessible obj = null;
		if (hl != null) {
			java.lang.Object anchor = 
				hl.getAccessibleActionAnchor(i);
			if (anchor instanceof javax.accessibility.Accessible) {
				AccessibleContext ac = 
				((javax.accessibility.Accessible) 
				          anchor).getAccessibleContext(); 
				obj = JavaBridge.getAccessibleObjectFactory().
					getAccessible (ac);
			}
		}
		return obj;
	}
	
	public String getURI (int i) {
		String s = "";
		if (hl != null) 
			s = hl.getAccessibleActionObject(i).toString();
		return s;
	}

	public boolean isValid () {
		return hl.isValid();
	}

	public void unImplemented () {}
	public void unImplemented2 () {}
	public void unImplemented3 () {}
	public void unImplemented4 () {}
}
