/*
 * java-access-bridge for GNOME 
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Accessibility;


import javax.accessibility.*;
import org.GNOME.Bonobo.*;

public class ToplevelImpl extends AccessibleImpl {

  int parentIndex;

  public ToplevelImpl () {
	  super ();
  }

  public ToplevelImpl(AccessibleContext ac, int index) {
	  super (ac);
	  this.parentIndex = index;
  }

  public void setParentIndex (int index) {
	  this.parentIndex = index;
  }

  public int getIndexInParent() {
	  return parentIndex;
  }

  public int getIndexInParentAlt() {
	  if (ac != null) {
		  javax.accessibility.Accessible parent = ac.getAccessibleParent();
		  if (parent != null) {
			  javax.accessibility.AccessibleContext pac = parent.getAccessibleContext();
			  // ugly, isn't it?
			  int i=0;
			  int n=pac.getAccessibleChildrenCount();
			  while (i<n) {
				  if (pac.getAccessibleChild(i).getAccessibleContext()
					  == ac) return i;
				  ++i;
			  }
		  }
	  }
	  return parentIndex; // should be -1, but this routine is buggy at the moment
  }

}
