/*
 * java-access-bridge for GNOME 
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Accessibility;

import javax.accessibility.*;
import org.GNOME.Bonobo.*;

class StateSetImpl extends UnknownImpl implements StateSetOperations {

	protected AccessibleStateSet stateSet;

	public StateSetImpl (AccessibleStateSet stateSet) {
		this.stateSet = stateSet;
		this.poa = JavaBridge.getRootPOA();
		tie = new StateSetPOATie (this, JavaBridge.getRootPOA());
		try {
			JavaBridge.getRootPOA().activate_object(tie);
		} catch (Exception e) {
			System.out.println (e);
		}
	}

	public StateSetImpl () {
		this.stateSet = new AccessibleStateSet();
		this.poa = JavaBridge.getRootPOA();
		tie = new StateSetPOATie (this, JavaBridge.getRootPOA());
		try {
			JavaBridge.getRootPOA().activate_object(tie);
		} catch (Exception e) {
			System.out.println (e);
		}
	}
	
	public boolean contains (StateType state) {
		boolean retval = false;
		AccessibleState testState = 
			StateTypeAdapter.accessibleStateFromStateType (state);
		if ((testState != null) && (stateSet != null))
			retval = stateSet.contains (testState);
		return retval;
	}

	public void add (StateType state) {
		stateSet.add (
			StateTypeAdapter.accessibleStateFromStateType (state));
	}

	public void remove (StateType state) {
		stateSet.remove (StateTypeAdapter.
				 accessibleStateFromStateType (state));
	}

	public boolean _equals (StateSet compareSet) {
		return compare(compareSet).isEmpty();
	}
	
	/* returns a 'difference set' */
	public StateSet compare (StateSet compareSet) {
		StateSet set = null;
		StateSetImpl impl = null;
		if (compareSet.isEmpty()) {
			impl = this;
		} else if (this.isEmpty()) {
			set = compareSet;
		} else {
			StateSetImpl differenceSet = new StateSetImpl ();
			StateType[] compareStates = compareSet.getStates(); 
			StateType[] ourStates = this.getStates();
			for (int i = 0; i < compareStates.length; ++i) {
				if (!this.contains(compareStates[i]))
					differenceSet.add (compareStates[i]);
			}
			for (int i = 0; i < ourStates.length; ++i) {
				if (!compareSet.contains(ourStates[i]))
					differenceSet.add (ourStates[i]);
			}
			impl = differenceSet;
		}
		if (impl != null) set = StateSetHelper.narrow (impl.tie());
		return set;
	}

	public boolean isEmpty () {
		return stateSet.toArray().length == 0;
	}

	public StateType[] getStates () {
		AccessibleState[] states = stateSet.toArray();
        if ( states != null ) {
            java.util.Vector stateTypes = new java.util.Vector(states.length + 1);
            for (int i = 0; i < states.length; ++i) {
                stateTypes.add (
                    new StateType (
                        StateTypeAdapter.
                        valueFromAccessibleState (states[i])));
		if (states[i] == AccessibleState.ENABLED) {
                    stateTypes.add ( new StateType (StateType._STATE_SENSITIVE));
		}
            }
            return (StateType[]) stateTypes.toArray( new StateType[0]);
        } 
        else {
            return new StateType[0];
        }
	}

	public void unImplemented () {}
	public void unImplemented2 () {}
	public void unImplemented3 () {}
	public void unImplemented4 () {}

	protected AccessibleStateSet getJavaStateSet() {
		return stateSet;
	}
}
