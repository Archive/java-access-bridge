/*
 * java-access-bridge for GNOME
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Accessibility;

import java.awt.Frame;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowAdapter;
import java.awt.event.AWTEventListener;
import java.util.List;
import java.util.LinkedList;
import java.util.Collection;
import java.util.Collections;
import java.util.Locale;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleRole;

public class ApplicationAccessible implements javax.accessibility.Accessible {
	AccessibleContext accContext = null;
	public ApplicationAccessible () {
		this.accContext = new AccessibleAppContext ();
	}
	public AccessibleContext getAccessibleContext () {
		return accContext;
	}

  public class AccessibleAppContext extends AccessibleContext {

	  List vtoplevels = Collections.synchronizedList (new LinkedList ());

	  private int toplevelCount () {
		  return vtoplevels.size();
	  }

	  private boolean isToplevel (java.lang.Object o) {
          if ( o instanceof java.awt.Window ||
		       o instanceof java.awt.Frame ||
               o instanceof java.awt.Dialog ) 
              return true;
		  return false;
	  }

	  final WindowListener l = new WindowAdapter() {
		  public void windowActivated(WindowEvent e) { 
			  // System.out.println ("Window activated.");
			  JavaBridge.dispatchWindowEvent (e.getSource(), "window:activate");
		  }
		  public void windowStateChanged(WindowEvent e) { 
			  // System.out.println ("Window state changes.");
			  JavaBridge.dispatchWindowEvent (e.getSource(), "object:state-changed");
		  }
		  public void windowDeiconifed(WindowEvent e) { 
			  // System.out.println ("Window deiconified.");
			  JavaBridge.dispatchWindowEvent (e.getSource(), "window:restore");
		  }
		  public void windowIconifieded(WindowEvent e) { 
			  // System.out.println ("Window iconified.");
			  JavaBridge.dispatchWindowEvent (e.getSource(), "window:minimize");
		  }
		  public void windowDeactivated(WindowEvent e) { 
			  // System.out.println ("Window deactivated.");
			  JavaBridge.dispatchWindowEvent (e.getSource(), "window:deactivate");
		  }
		  public void windowClosed(WindowEvent e) { 
			  // System.out.println ("Window closed.");
              java.awt.Window win = e.getWindow();
			  if (isToplevel( win )) {
				  vtoplevels.remove( win );
			  }
			  JavaBridge.dispatchWindowEvent (e.getSource(), "window:destroy");
			  JavaBridge.dispatchEvent (getAccessibleContext(),
						    "object:children-changed:remove",
						    0,
						    0);
			  if (toplevelCount() == 0) JavaBridge.exit (); 
		  }
		  public void windowOpened(WindowEvent e) {
			  // System.out.println ("Window opened.");
              java.awt.Window win = e.getWindow();
			  if (isToplevel( win )) {
				  // Avoid duplication.
                  if ( !vtoplevels.contains( win ) ) {
					  vtoplevels.add( win );
		  			  javax.accessibility.Accessible a;
					  a = (javax.accessibility.Accessible) win;
					  // Ensure consistency of parent.
					  a.getAccessibleContext().setAccessibleParent( ApplicationAccessible.this);
                  }
			  }
			  else {
			  	System.out.println("WARNING: Unhandled Window type : " + win.toString());
			  }
			  JavaBridge.dispatchWindowEvent (e.getSource(), "window:create");
			  javax.accessibility.Accessible a_parent;
              if ( e.getSource() instanceof javax.accessibility.Accessible ) {
                  JavaBridge.traverseMenus(
                                  ((Accessible)e.getSource()).getAccessibleContext() );
              }
			  JavaBridge.dispatchEvent (getAccessibleContext(),
							"object:children-changed:add",
							0,
							0);
		  }
	  };
	  AWTEventListener globalListener = new AWTEventListener () {
		  public void eventDispatched (java.awt.AWTEvent e) {
			  if (e instanceof WindowEvent) {
				  // System.out.println("Window Event Occurred : "+e.toString());
				  switch (e.getID()) {
				  case java.awt.event.WindowEvent.WINDOW_OPENED:
					  ((java.awt.event.WindowEvent) e).getWindow().addWindowListener(l);
					  break;
				  case java.awt.event.WindowEvent.WINDOW_LOST_FOCUS:
                      			  JavaBridge.dispatchFocusEvent(null);
					  break;
				  default:
					  break;
				  }
			  } else if (e instanceof java.awt.event.FocusEvent) {
                  // System.err.println("FocusEvent received");
				  switch (e.getID()) {
				  case java.awt.event.FocusEvent.FOCUS_GAINED:
                      JavaBridge.dispatchFocusEvent(e.getSource());
					  break;
				  default:
					  break;
				  }
			  } 
		  }
	  };
	  java.awt.Toolkit toolkit = java.awt.Toolkit.getDefaultToolkit ();

	  public AccessibleAppContext() {
		  super();
		  toolkit.addAWTEventListener(globalListener,
					      java.awt.AWTEvent.WINDOW_EVENT_MASK 
					      | java.awt.AWTEvent.FOCUS_EVENT_MASK);
		  toolkit.getSystemEventQueue().push (JavaBridge.getEventQueue ());
		  // Ensure that toplevels report parent correctly
                  for ( int i = 0; i < getAccessibleChildrenCount(); i++ ) {
                  	javax.accessibility.Accessible child = getAccessibleChild (i);
		  }
	  }

	  public Locale getLocale() {
		  return null;
	  }

	  public javax.accessibility.Accessible getAccessibleChild(int i) {
		  javax.accessibility.Accessible a = null;
		  if ( i < toplevelCount() ) {		  	  
			  a = (javax.accessibility.Accessible) vtoplevels.get (i);
			  // Ensure consistency of parent.
			  a.getAccessibleContext().setAccessibleParent( ApplicationAccessible.this);
		  }
		  return a;
	  }
	  public javax.accessibility.Accessible getAccessibleParent() {
		  // System.out.println ("app has no parent");
		  return null;
	  }
	  public int getAccessibleChildrenCount() {
		  return toplevelCount();
	  }

	  public int getAccessibleIndexInParent() {
		  return -1;
	  }
	  public javax.accessibility.AccessibleStateSet getAccessibleStateSet() {
		  return new javax.accessibility.AccessibleStateSet();
	  }
	  public javax.accessibility.AccessibleRole getAccessibleRole() {
		  return javax.accessibility.AccessibleRole.UNKNOWN;
	  }
	  public String getAccessibleName() {
		  // TODO: find a better way of getting this than the hack below
          // Changed to find a string other than either null or blank... 
		  String ret_str = null;
          String s;
		  if (toplevelCount() > 0) {
              for ( int i = 0; i < getAccessibleChildrenCount(); i++ ) {
                  javax.accessibility.Accessible 
                      child = getAccessibleChild (i);
                  if (child != null) {
                      s = child.getAccessibleContext().
                          getAccessibleName ();
                      if ( s != null && s.length() != 0 ) {
                          ret_str = s;
                          break;
                      }
                  }
              }
		  }
		  if ( ret_str == null )
              ret_str = "Java Application";
		  return ret_str;
	  }
  	  public String getAccessibleDescription() {
		  return "an accessible Java app";
	  }
  }

}
