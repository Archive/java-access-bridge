/*
 * java-access-bridge for GNOME
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Accessibility;

import javax.accessibility.*;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Dimension;

public class ComponentImpl extends org.GNOME.Bonobo.UnknownImpl implements ComponentOperations {

	AccessibleComponent accComponent;
	AccessibleContext   ac;
	java.awt.Component  component; // used only by MDI layer implementation
	javax.swing.JLayeredPane layeredPane;

	// TODO: initialize component and layeredPane

	public ComponentImpl(AccessibleContext ac) {
		super();
		accComponent = ac.getAccessibleComponent();
		this.ac = ac;
		this.component = null;
		this.layeredPane = null;
		this.poa = JavaBridge.getRootPOA();
		tie = new ComponentPOATie (this, JavaBridge.getRootPOA());
		try {
			JavaBridge.getRootPOA().activate_object(tie);
		} catch (Exception e) {
			System.out.println (e);
		}
	}

  public boolean contains (int x, int y, short coord_type) {

		Point p = new Point(x, y);
		
		if(accComponent != null) 
			return accComponent.contains(p);
		return false;
	}

  public org.GNOME.Accessibility.Accessible getAccessibleAtPoint (int x, int y, short coord_type) {

		org.GNOME.Accessibility.Accessible accessibleAtPoint = null;

		if(accComponent != null) {
			javax.accessibility.Accessible acc = accComponent.getAccessibleAt(new Point(x,y));
		    if (acc != null) {
			    accessibleAtPoint = JavaBridge.getAccessibleObjectFactory().
				                    getAccessible(acc.getAccessibleContext());
		    } 
	    }
        return accessibleAtPoint;
	}

	public org.GNOME.Accessibility.BoundingBox getExtents (short coord_type) {

		BoundingBox b = null;
		if (accComponent != null ) {
			Dimension d = null;
			try {
				d = accComponent.getSize();
			} catch (Exception ex) {} // XXX: workaround for VM bug!
			Point p = accComponent.getLocationOnScreen();
			if (d == null) d = new Dimension(0, 0);
			if (p == null) p = new Point(-1, -1);
			if (coord_type == 0) {
				b =  new BoundingBox (p.x, p.y, 
						      (int) d.getWidth(), 
						      (int) d.getHeight());
			} else {
				Point win_loc = getToplevelLocationOnScreen();
				b = new BoundingBox (p.x-win_loc.x, p.y-win_loc.y, 
						     (int) d.getWidth(), 
						     (int) d.getHeight());
			}
		} 
		if (b == null) System.out.println ("Null bounds!");
		return b;
	}

	private Point getToplevelLocationOnScreen () {
		Point p = new Point(0,0);
		javax.accessibility.Accessible parent = ac.getAccessibleParent();
		javax.accessibility.AccessibleContext context = ac;
		javax.accessibility.AccessibleComponent component =
			context.getAccessibleComponent();
		while (parent != null && component != null) {
			Point cp = component.getLocation();
			p.x += cp.x;
			p.y += cp.y;
			parent = context.getAccessibleParent();
			context = parent.getAccessibleContext();
			component = context.getAccessibleComponent();
		}
		return p;
	}

	public void getPosition (org.omg.CORBA.IntHolder x, org.omg.CORBA.IntHolder y, short coord_type) {

		if (accComponent != null ) {
			Point p = accComponent.getLocationOnScreen();
			if (coord_type != 0) {
				Point toplevelp = getToplevelLocationOnScreen();
				p.x -= toplevelp.x;
				p.y -= toplevelp.y;
			}
			x.value = p.x;
			y.value = p.y;
		}
	}

	public void getSize (org.omg.CORBA.IntHolder width, org.omg.CORBA.IntHolder height){
    if (accComponent != null ) {
			Dimension d = null;
			try {
				d = accComponent.getSize();
			} catch (Exception ex) {
				d = new Dimension (0, 0);
			}
			width.value = d.width;
			height.value = d.height;
		}
	}

  public boolean grabFocus () {
		
		if (accComponent != null) {
			accComponent.requestFocus();
			return true;
		} else
			return false;
	}

  public void registerFocusHandler (org.GNOME.Accessibility.EventListener handler) {
//		if(accComponent != null )
//		accComponent.addFocusListener();
  }

  public void deregisterFocusHandler (org.GNOME.Accessibility.EventListener handler) {
//   if(accComponent != null )
//   accComponent.removeFocusListener();
  }

	public org.GNOME.Accessibility.ComponentLayer getLayer() {
		if (ac == null) return ComponentLayer.LAYER_INVALID;
		AccessibleRole role = ac.getAccessibleRole();
		if (role == AccessibleRole.MENU ||
		    role == AccessibleRole.MENU_ITEM || 
		    role == AccessibleRole.POPUP_MENU ||
		    role == AccessibleRole.MENU)
			return ComponentLayer.LAYER_POPUP;
		else if (role == AccessibleRole.INTERNAL_FRAME)
			return ComponentLayer.LAYER_MDI;
		else if (role == AccessibleRole.GLASS_PANE)
			return ComponentLayer.LAYER_OVERLAY;
		else if (role == AccessibleRole.CANVAS ||
			role == AccessibleRole.ROOT_PANE ||
			role == AccessibleRole.LAYERED_PANE) 
			return ComponentLayer.LAYER_CANVAS;
		else return ComponentLayer.LAYER_WIDGET;
	}

	public short getMDIZOrder () {
		if (getLayer () != ComponentLayer.LAYER_MDI)
			return 0;
		if (layeredPane != null && component != null)
			return (short) layeredPane.getPosition (component);
		return 0;
	}

	public double getAlpha () {
		return 1.0;
	}

	public void unImplemented () {}
	public void unImplemented2 () {}
	public void unImplemented3 () {}
}
