/*
 * java-access-bridge for GNOME
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Accessibility;
import org.GNOME.Bonobo.*;
import javax.accessibility.*;
import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Enumeration;

public class TextImpl extends UnknownImpl implements TextOperations {

	AccessibleText accText;
	AccessibleEditableText accEditableText;
	AccessibleContext ac;

    // TODO - Remove need for this.
    //
    // Until JAA implements directly, we've agreed with SO/OOo to pass the
    // value through.
    static final int LINE_PART = javax.accessibility.AccessibleText.SENTENCE + 1;

	public class StringStruct {
		public int startOffset;
		public int endOffset;
		public String string;
	};

	public TextImpl (AccessibleContext ac) {
		super ();
		accText = ac.getAccessibleText();
		accEditableText = ac.getAccessibleEditableText();
		this.ac = ac;
		this.poa = JavaBridge.getRootPOA();
		tie = new TextPOATie (this, JavaBridge.getRootPOA());
		try {
			JavaBridge.getRootPOA().activate_object(tie);
		} catch (Exception e) {
			System.err.println (e);
		}

        // XXX To enable support for insert/delete/replace qualification of
        // text-changed events we need to know the initial amount of text in
        // the text object.
        JavaBridge.setInitialTextSize( ac, characterCount() );
	}

	public int characterCount () {
	
		int count = 0;
	
        try {
            if( accText != null )
                count = accText.getCharCount();
        } catch ( Exception e) {
            e.printStackTrace();
        }

		return count;	
	}

	public int caretOffset () {

		int offset = 0;
		
        try {
            if( accText != null )
                offset = accText.getCaretPosition();
        } catch ( Exception e ) {
            e.printStackTrace();
        }
		
        if ( offset < 0 )
            return 0;
        else 
            return offset;
	}

	public String getText (int startOffset, int endOffset) {
//        System.err.println("getText ( " + startOffset + ", " + endOffset + ")");
//        (new Exception("Anything?")).printStackTrace();

		String text = "";
        try {
            int nchars = accText.getCharCount();
            if (startOffset < 0) startOffset = 0;
            if (endOffset >= nchars || endOffset == -1) 
                endOffset = nchars;
            else if (endOffset < -1) 
                endOffset = 0;
            if (accEditableText != null) {
                try {
                    String textRange = 
                        accEditableText.getTextRange (startOffset, 
                                          endOffset);
                    if (textRange != null)
                        text = textRange;
		    // System.out.println ("Editable textRange: " + textRange);
		    //System.out.println ("Last char: " + 
		    //			accEditableText.getAtIndex (javax.accessibility.AccessibleText.CHARACTER, endOffset-1));
                } catch (Exception ex) {
                    // System.err.println ("Error getting text range: " + ex);
                    ex.printStackTrace();
                }
            }
            else if (accText != null) { 
                int offset = startOffset;
                String word = accText.getAtIndex (
                    javax.accessibility.AccessibleText.WORD, 
                    startOffset);
                // go char-by-char until word changes,
                while (offset < endOffset && word.equals (accText.getAtIndex (
                       javax.accessibility.AccessibleText.WORD, offset) ) ) {
                    String character = accText.getAtIndex (
                        javax.accessibility.AccessibleText.CHARACTER, 
                        offset);
                    ++offset;
                    if (character != null)
                        text += character; 
    // TODO: charbuff would be faster here
                }
                // then word by word until offset crosses
                while (offset < endOffset) {
		    int wordlen;
                    word = accText.getAtIndex (
                        javax.accessibility.AccessibleText.WORD, 
                        offset);
		    wordlen = word.length ();
		    // System.out.println ("word [" + word + "] len=" + wordlen);
                    offset += wordlen;
                    if (offset > endOffset) 
                        text += word.substring (0,
                             word.length() - (offset - endOffset));
                    else
                        text += word;
                } // TODO: sentence-by-sentence for even longer spans.
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        // System.err.println("TEXT: |" + text + "|");
		return text; 	
	}

	public boolean setCaretOffset (int offset) {
		if (accEditableText != null) {
			accEditableText.selectText (offset, offset);
			return true;
		}
		return false;
	}

    // Used to decide whether two characters are on the same line. this is
    // done by checking if for the intersection of the Y axis between
    // rectangles.
    private boolean overlapsVertically( Rectangle r1, Rectangle r2 ) {
        if ( r1 == null | r2 == null ) {
            return false;
        }

        // Try save some time if they are the same baseline.
        if ( r1.y == r2.y ) {
            return true;
        }
    
        int y1 = r1.y;
        int y2 = r1.y + r1.height;

        int Y1 = r2.y;
        int Y2 = r2.y + r2.height;

        if ( ( y2 == Y2 ) || 
             ( y1 > Y1 && y1 < Y2 ) ||
             ( Y1 > y1 && Y1 < y2 ) ||
             ( y2 > Y1 && y2 < Y2 ) ||
             ( Y2 > y1 && Y2 < y2 )  ) {
            return true;
        }

        return false;
    }

	private StringStruct getLine (javax.accessibility.AccessibleText accText,
				      int offset,
				      TEXT_BOUNDARY_TYPE type) {
	    //System.err.println("getLine ( " + offset + " )");
		String string = "";
		StringStruct lineStruct = new StringStruct ();
        int start = offset;
        int end = offset;

        try {
            int lastOffset = accText.getCharCount ();
            Rectangle bounds = accText.getCharacterBounds (offset);
            Rectangle currentBounds = null;
            if (bounds == null && offset > 0) {
                bounds = accText.getCharacterBounds (offset - 1);
            }

            while (start > 0) {
		currentBounds = accText.getCharacterBounds (--start);
		if (overlapsVertically( bounds, currentBounds )) {
		    continue;
		}
		else {
		    ++start;
		    break;
		}
	    }
            currentBounds = bounds;
            while (end < lastOffset && 
                   overlapsVertically( bounds, currentBounds )) {
                currentBounds = accText.getCharacterBounds (++end);
            } 
            //System.out.println ("getting text...(" + start + ", " + end + ")");
            string = this.getText (start, end);
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
	if (type == TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_LINE_END) {
	    if (start > 0)
		start--;
	}
        lineStruct.startOffset = start;
        lineStruct.endOffset = end;
        if (string == "") 
            lineStruct.string = "";
        else 
            lineStruct.string = string;

        return lineStruct;
	}

	private String concatenateText (javax.accessibility.AccessibleText accText,
					String text, 
					TEXT_BOUNDARY_TYPE type,
					int offset) {
        try {
            String string;
            //System.err.println("concatenateText: [" + text + "] type = " + typeToString( type) +
            //                   "; offset = " + offset );
            int java_boundary_type = boundaryTypeConvert (type); 
            if (offset < 0 || offset > accText.getCharCount()) return "";
            if (type == TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_WORD_START ||
                type == TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_SENTENCE_START) {
                string = accText.getBeforeIndex(java_boundary_type, offset);
                if (string != null) text = string + text; 
            } else {
                string = accText.getAfterIndex(java_boundary_type, offset);
                if (string != null) text += string;
            }
	    //System.err.println ("Concatenated: [" + text + "]");
        } 
        catch ( Exception e ) {
            e.printStackTrace();
        }
		return text;
	}
			
	private String concatenateWhitespace (javax.accessibility.AccessibleText accText,
					      String text, 
					      TEXT_BOUNDARY_TYPE type,
					      int offset) {
        try {
            String string;
            int java_boundary_type = boundaryTypeConvert (type);
            if (offset < 0 || offset > accText.getCharCount()) return "";
            //System.err.println("concatenateWhitespace: [" + text + "] type = " + typeToString( type) +
            //                   "; offset = " + offset );
            if (type == TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_WORD_START) {
                string = accText.getAfterIndex(java_boundary_type, offset);
		//System.err.println ("AfterIndex string: [" + string + "]");
		if (string != null)
                    text += string; 
            } else if (type == TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_WORD_END) {
                string = accText.getBeforeIndex(java_boundary_type, offset);
		if (string != null)
		    text = string + text;
            } 
	    //System.err.println ("Concatenated: [" + text + "]");
        } 
        catch ( Exception e ) {
            e.printStackTrace();
        }
		return text;
	}

	private int boundaryTypeConvert (org.GNOME.Accessibility.TEXT_BOUNDARY_TYPE type) {
		int boundary_type;
        if (type == TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_LINE_START
            || type == TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_LINE_END) {
            boundary_type = LINE_PART;
		} else if (type == TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_WORD_START
		    || type == TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_WORD_END) {
			boundary_type = javax.accessibility.AccessibleText.WORD;
		} else if (type == TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_SENTENCE_START
			   || type == TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_SENTENCE_END) {
			boundary_type = 
				javax.accessibility.AccessibleText.SENTENCE;
		} else {	 
			boundary_type = javax.accessibility.AccessibleText.CHARACTER; 
	        }
		return boundary_type;
	}

	private StringStruct findOffsets (javax.accessibility.AccessibleText accText,
					  String text, int offset, boolean leading_ws) {
	    //System.err.println("findOffsets (["
	    //		       + text + "], "
	    //		       + offset + ") " );
	    StringStruct result = new StringStruct ();
	    String enclosingString;
	    int len = text.length ();
	    int index;
	    
	    // 2) try and get string from offset-len to offset+len
	    enclosingString = this.getText (offset-len, offset+len);
	    // System.err.println("======" + (offset-len) + "===" + (offset+len) + "===");
	    // System.err.println("enclosingString for ["+ text + "] = " + enclosingString );
	    // System.err.println("==========================" );
	    
	    // 3) find text in resulting string 
	    index = enclosingString.indexOf (text);
	    // System.err.println("index = " + index );
	    // Java bug workaround; try to find string w/o newline
	    if (index == -1) {
		if (text.endsWith ("\n")) {
		    // System.err.println ("FAILED TO FIND string ending in newline!");
		    text = text.substring (0, text.length () - 1);
		    index = enclosingString.indexOf (text);
		}
	    }
	    // creep forward to add whitespace, if needed
	    if (leading_ws) {
		while ((index > 0) && Character.isWhitespace (enclosingString.charAt (index - 1))) 
		    --index;
	    }

	    // 4) do index math to determine where original string's offsets lie.
	    offset -= len; 
	    if ( offset < 0 ) 
		offset = 0;
	    offset += index; 
	    // System.err.println("offset = " + offset );
	    
	    // 5) return modified StringStruct
	    result.startOffset = offset;
	    result.endOffset = offset + len;
	    // System.err.println("startOffset = " + result.startOffset );
	    // System.err.println("endOffset = " + result.endOffset );
	    
	    return result;
	}
    
	private StringStruct getBoundedString (
		             javax.accessibility.AccessibleText accText,
			     org.GNOME.Accessibility.TEXT_BOUNDARY_TYPE type,
			     int offset) {
		int boundary_type;
        String text = null;
		StringStruct stringStruct = new StringStruct ();
        try {
            if (type == TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_LINE_START
                || type == TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_LINE_END) {
                // Attempt using the part value, else work around lack of
                // support for LINE part.
                boundary_type = boundaryTypeConvert (type); 
                text = accText.getAtIndex(boundary_type, offset);
                if ( text == null )
                    return getLine (accText, offset, type);
            } else {	 
                boundary_type = boundaryTypeConvert (type); 
                text = accText.getAtIndex(boundary_type, offset);
		// System.err.println ("getBoundedString: [" + text + "]");
            }
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
		//stringStruct.startOffset = 0;
		//stringStruct.endOffset = -1;
        if (text == null) 
            stringStruct.string = "";
        else {
            stringStruct = findOffsets (accText, text, offset, false);
            stringStruct.string = text;
        }
		//System.out.println ("text found: "+stringStruct.string);
		return stringStruct;
	}

	public String getTextBeforeOffset (int offset,
		   org.GNOME.Accessibility.TEXT_BOUNDARY_TYPE type, 
		   org.omg.CORBA.IntHolder startOffset, 
		   org.omg.CORBA.IntHolder endOffset) {
		// TODO: debug
        String text = "";
        try {
	    boolean leading_ws = false;
            if (accText != null) {
                StringStruct stringStruct = getBoundedString (accText, type, offset);
                if (stringStruct.string != null) 
                    text = stringStruct.string;
                if (type != TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_LINE_START
                       && type != TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_LINE_END
                       && type != TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_CHAR) {
                    if (text.trim().length() == 0) {
                        text = concatenateText (accText, text, type, 
                            offset-stringStruct.string.length());
                    } else { 
                        text = concatenateWhitespace (accText, text, type, 
                                offset-stringStruct.string.length());
			if (type == TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_SENTENCE_END) {
			    // trim trailing whitespace
			    text = text.trim();
			    // and add leading whitespace
			    leading_ws = true;
			}
                    }
                    stringStruct = findOffsets (accText, text, offset, leading_ws);
                }
                startOffset.value = stringStruct.startOffset;
                endOffset.value = stringStruct.endOffset;
            }
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
	    // System.out.println ("TEXT [before]: |" + text + "|");
	    return text;
	}

	public String getTextAtOffset (int offset, 
	               org.GNOME.Accessibility.TEXT_BOUNDARY_TYPE type, 
		       org.omg.CORBA.IntHolder startOffset, 
		       org.omg.CORBA.IntHolder endOffset) {
	    String text = "";
//        System.err.println("getTextAtOffset ( "
//                            + offset + ", "
//                            + type.value() + ", "
//                            + startOffset.value + ", "
//                            + endOffset.value + ") " );
        try {
	    boolean leading_ws = false;
            if (accText != null) {
                StringStruct stringStruct = getBoundedString (accText, type, offset);
                if (stringStruct.string != null) 
                    text = stringStruct.string;
		//System.err.println ("getTextAtOffset [" + offset + "]: " + text);
                if (type != TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_LINE_START
                       && type != TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_LINE_END
                       && type != TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_CHAR) {
                    if (text.trim().length() == 0) {
                        text = concatenateText (accText, text, type, offset);
                    } else { 
                        text = concatenateWhitespace (accText, text, type, offset);
			if (type == TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_SENTENCE_END) {
			    // trim trailing whitespace
			    text = text.trim();
			    // and add leading whitespace
			    leading_ws = true;
			}
                    }
                    stringStruct = findOffsets (accText, text, offset, leading_ws);
                }
                startOffset.value = stringStruct.startOffset;
                endOffset.value = stringStruct.endOffset;
            }
        } 
        catch ( Exception e ) {
            e.printStackTrace();
        }
//        System.err.println ("TEXT: |" + text + "|");
	    return text;
	}
	
	public String getTextAfterOffset (int offset, 
			  org.GNOME.Accessibility.TEXT_BOUNDARY_TYPE type, 
			  org.omg.CORBA.IntHolder startOffset, 
			  org.omg.CORBA.IntHolder endOffset) {
		// TODO: debug
	    String text = "";
        try {
	    boolean leading_ws = false;
            if (accText != null) {
                StringStruct stringStruct = getBoundedString (accText, type, offset);
                if (stringStruct.string != null) 
                    text = stringStruct.string;
                if (type != TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_LINE_START
                       && type != TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_LINE_END
                       && type != TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_CHAR) {
                    if (text.trim().length() == 0) {
                        text = concatenateText (accText, text, type, 
                            offset+stringStruct.string.length());
                    } else { 
                        text = concatenateWhitespace (accText, text, type,
                              offset+stringStruct.string.length());
			if (type == TEXT_BOUNDARY_TYPE.TEXT_BOUNDARY_SENTENCE_END) {
			    // trim trailing whitespace
			    text = text.trim();
			    // and add leading whitespace
			    leading_ws = true;			
			}
                    }
                    stringStruct = findOffsets (accText, text, offset, leading_ws);
                }
                startOffset.value = stringStruct.startOffset;
                endOffset.value = stringStruct.endOffset;
            }
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
	    // System.out.println ("TEXT [after]: |" + text + "|");
	    return text;
	}

	public int getCharacterAtOffset (int offset) {
        try {
            if (accText != null) {
                return (int) accText.getAtIndex(
                    javax.accessibility.AccessibleText.CHARACTER, 
                    offset).charAt(0);
            }
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
		return 0;

	}

        private Object toJavaTextAttribute (String name)
	{
            if ( name.equals( "family-name" ) ) {
		return "family";
	    }
            else if ( name.equals( "style" ) ) {
                return "italic";
            }
	    else if ( name.equals( "weight" ) ) {
		return "bold";
	    }
	    else if ( name.equals ( "fg-color" ) ) {
		return "foreground";
	    }
	    else if ( name.equals ( "bg-color" ) ) {
		return "background";
	    }
	    else return name;
	}

        private String fromJavaTextAttribute (Object name, Object value)
	{
	    String result = "";
            String nameStr = name.toString();
            String valueStr = value.toString();
            // System.out.println("name = " + nameStr + "; value = " + valueStr );
            if ( nameStr.equals( "family" ) ) {
                result = "family-name:" + valueStr;
            }
            else if ( nameStr.equals( "size" ) ) {
                result = nameStr + ":" + valueStr;
            }
            else if ( nameStr.equals( "italic" ) ) {
                if ( Boolean.valueOf( valueStr ).booleanValue() ) {
                    result = "style:italic";
                }
                else {
                    result = "style:normal";
                }
            }
            else if ( nameStr.equals( "bold" ) ) {
                if ( Boolean.valueOf( valueStr ).booleanValue() ) {
                    result = "weight:700";
                }
                else {
                    result = "weight:400";
                }
            }
            else if ( nameStr.equals( "foreground" ) || nameStr.equals( "color" ) ) {
                if ( value instanceof java.awt.Color ) {
                    Color c = (Color)value;
                    result = "fg-color:";
                    result += (c.getRed() * 256) + c.getRed();
                    result += ",";
                    result += (c.getGreen() * 256) + c.getGreen();
                    result += ",";
                    result += (c.getBlue() * 256) + c.getBlue();
                }
            }
            else if ( nameStr.equals( "background" ) ) {
                if ( value instanceof java.awt.Color ) {
                    Color c = (Color)value;
                    result = "bg-color:";
                    result += (c.getRed() * 256) + c.getRed();
                    result += ",";
                    result += (c.getGreen() * 256) + c.getGreen();
                    result += ",";
                    result += (c.getBlue() * 256) + c.getBlue();
                }
            }
            else if ( nameStr.equals( "underline" ) ) {
                result = "underline:";
                if ( Boolean.valueOf( valueStr ).booleanValue() ) {
                    result += "single";
                }
                else {
                    result += "none";
                }
            }
            else if ( nameStr.equals( "strikethrough" ) ) {
                result = "strikethrough:" + Boolean.valueOf( valueStr ).booleanValue();
            }
            else {  // Default to Strings
                result = nameStr + ":" + valueStr;
            }
	    return result;
	}

	private String attributeSetConvert (javax.swing.text.AttributeSet attributes) {
		// TODO: harmonize attribute names with atk-bridge/at-spi
		String result = "";
		Enumeration names = attributes.getAttributeNames ();
		while (names.hasMoreElements ()) {
			Object name = names.nextElement ();
			Object value = attributes.getAttribute (name);
            // System.out.println("attributeSetConvert: name is type : " +
            //                     name.getClass().getName() );
            // System.out.println("attributeSetConvert: value is type : " +
            //                     value.getClass().getName() );
			result += fromJavaTextAttribute (name, value);
			if (names.hasMoreElements ()) result += "; ";
		}
        // System.out.println("attributeSetConvert : returns : >>" + result + "<<" );
		return result;
	}

	public String getAttributes (int offset, 
				     org.omg.CORBA.IntHolder startOffset, 
				     org.omg.CORBA.IntHolder endOffset) {
        int start = offset;
        int end = offset;
        int last = -1;
        try {
//            System.err.println("getAttributes( "+ offset + " ) : BEGINS");
            if (accText != null) {
                javax.swing.text.AttributeSet attributes = 
                    accText.getCharacterAttribute(offset);
                last = accText.getCharCount ();

                javax.swing.text.AttributeSet tmp_attrs;
                start--;
//                System.err.println("start = " + start );
                if ( start > 0 ) {
                    tmp_attrs = accText.getCharacterAttribute(start);
                    while ( start > 0 &&
                            (( attributes == null && attributes == tmp_attrs ) ||
                             ( attributes != null && tmp_attrs != null &&
                               attributes.isEqual ( tmp_attrs ) ) ) ) {
                        tmp_attrs = accText.getCharacterAttribute(--start);
//                        System.err.println("start = " + start );
                    }
                }

                end++;
//                System.err.println("end = " + end );
                if ( end < last ) {
                    tmp_attrs = accText.getCharacterAttribute(end);
                    while ( end < last &&
                            (( attributes == null && attributes == tmp_attrs ) ||
                             ( attributes != null && tmp_attrs != null &&
                               attributes.isEqual ( tmp_attrs ) ) ) ) {
                        tmp_attrs = accText.getCharacterAttribute(++end);
//                        System.err.println("end = " + end );
                    }
                }
                startOffset.value = ++start;
                endOffset.value = end;
//                System.err.println("startOffset.value = " + startOffset.value );
//                System.err.println("endOffset.value = " + endOffset.value );
//                System.err.println("getAttributes() : FINISH");
                if ( attributes != null )
                    return attributeSetConvert (attributes);
                else
                    return "";
            }
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
        // Catch-all failed
        // XXX - TODO - What should we really use here ....?
        startOffset.value = offset;
        if ( last < 0 )
            endOffset.value = offset + 1;
        else 
            endOffset.value = last;
//        System.err.println("startOffset.value (Catchall)= " + startOffset.value );
//        System.err.println("endOffset.value (Catchall)= " + endOffset.value );
//        System.err.println("getAttributes() : end");
        return "";
	}

	private Point getToplevelLocationOnScreen () {
		Point p = new Point(0,0);
		javax.accessibility.Accessible parent = ac.getAccessibleParent();
		javax.accessibility.AccessibleContext context = ac;
		javax.accessibility.AccessibleComponent component =
			context.getAccessibleComponent();
		while (parent != null && component != null) {
			Point cp = component.getLocation();
			p.x += cp.x;
			p.y += cp.y;
			parent = context.getAccessibleParent();
			context = parent.getAccessibleContext();
			component = context.getAccessibleComponent();
		}
		return p;
	}

	public void getCharacterExtents (int offset, 
					 org.omg.CORBA.IntHolder x, 
					 org.omg.CORBA.IntHolder y, 
					 org.omg.CORBA.IntHolder width, 
					 org.omg.CORBA.IntHolder height, 
					 short coordType) {
        try { 
            java.awt.Rectangle r = null;
            if (accText != null) {
                r = accText.getCharacterBounds(offset);
                if (r == null) {
                    if (offset > 0) 
                        r = accText.getCharacterBounds(offset-1);
                    if (r == null) return; // ???
                    r.height = 0;
                    r.width = 0;
                }
                try {
                    AccessibleComponent component = 
                        ac.getAccessibleComponent ();
                    Point p = component.getLocationOnScreen ();
                    r.x += p.x;
                    r.y += p.y;
                } 
                catch (Exception ex) {
                    ex.printStackTrace();
                } // not a component ???
                if (coordType != 0) {
                    Point win_loc = 
                        getToplevelLocationOnScreen();
                    r.x -= win_loc.x;
                    r.y -= win_loc.y;
                }
                x.value = r.x;
                y.value = r.y;
                height.value =  r.height;
                width.value  =  r.width;
            }
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
	}

	public int getOffsetAtPoint (int x, int y, short coordType) {
	
		int offset = 0;

        try {
            if( accText != null )
                offset = accText.getIndexAtPoint(new Point(x, y));
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
		return offset;
	}

	public int getNSelections () {
        try {
            if (accText != null) {
                String s = accText.getSelectedText ();
                return (s == null || s.length() == 0) ? 0 : 1;
            }
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
		return 0;
	}

	public void getSelection (int selectionNum, 
				  org.omg.CORBA.IntHolder startOffset, 
				  org.omg.CORBA.IntHolder endOffset) {
        try {
            if (selectionNum ==0) {
                startOffset.value = 
                    accText.getSelectionStart();
                endOffset.value = 
                    accText.getSelectionEnd();
            } else {
                startOffset.value = 0;	
                endOffset.value = 0;	
            }
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
	}

	public boolean addSelection (int startOffset, int endOffset) {
        try {
            if (accEditableText != null && getNSelections() == 0) {
                return setSelection (0, startOffset, endOffset);
            }
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
		return false;
	}

	public boolean removeSelection (int selectionNum) {
        try {
            if (accEditableText != null && selectionNum == 0) {
                accEditableText.selectText (0, 0);
                return true;
            }
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
		return false;
	}

	public boolean setSelection (int selectionNum, int startOffset, int endOffset) {
        try {
            if (accEditableText != null && selectionNum == 0) {
                accEditableText.selectText (startOffset, endOffset);
                return true;
            }
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
		return false;
	}

   public void getRangeExtents (int startOffset, int endOffset,
                                 org.omg.CORBA.IntHolder x,
                                 org.omg.CORBA.IntHolder y,
                                 org.omg.CORBA.IntHolder width,
                                 org.omg.CORBA.IntHolder height,
                                 short coordType) {
        /* coordType:
           If 0, the above coordinates are reported in pixels relative
           to corner of the screen; if 1, the coordinates are reported
           relative to the corner of the containing toplevel window.
        */
        if (coordType != 0 && coordType != 1) {
            return;
        }

        if (accText instanceof AccessibleExtendedText) {
            // getTextBounds returns bounds relative to the parent container.
            AccessibleExtendedText accExtendedText = (AccessibleExtendedText)accText;
            Rectangle rect = accExtendedText.getTextBounds(startOffset, endOffset);
            if (rect == null) {
                return;
            }
            AccessibleContext accParent =
                ac.getAccessibleParent().getAccessibleContext();
            AccessibleComponent accComponent = (AccessibleComponent)accParent;
            Point parentLocation = accComponent.getLocationOnScreen();

            if (coordType == 0) {
                // getTextBounds returns a location relative to the parent
                // container. Add the location of the parent container relative
                // to the top-right corner of the screen.
                x.value = rect.x + parentLocation.x;
                y.value = rect.y + parentLocation.y;
                width.value = rect.width;
                height.value = rect.height;
            } else {
                // Same as for (coordType == 0) above, but subtract the location
                // of the top-level container.
                Point topLocation = getToplevelLocationOnScreen();
                x.value = rect.x + parentLocation.x - topLocation.x;
                y.value = rect.y + parentLocation.y - topLocation.y;
                width.value = rect.width;
                height.value = rect.height;
            }
        }
    }


    public org.GNOME.Accessibility.TextPackage.Range[] getBoundedRanges (
                                int x, int y, int width, int height, short coordType, 
                                org.GNOME.Accessibility.TEXT_CLIP_TYPE xClipType, 
                                org.GNOME.Accessibility.TEXT_CLIP_TYPE yClipType) {
        return new org.GNOME.Accessibility.TextPackage.Range[0];
    }

	public String getDefaultAttributes ()  {

		return "";
    }
        public String[] getDefaultAttributeSet ()  {

                return null;
    }

        public String[] getAttributeRun(int p1,org.omg.CORBA.IntHolder p2,org.omg.CORBA.IntHolder p3,boolean p4) {
                return null;
    }

        public String getAttributeValue (int offset,
					 java.lang.String name,
					 org.omg.CORBA.IntHolder startOffset,
					 org.omg.CORBA.IntHolder endOffset,
					 org.omg.CORBA.BooleanHolder defined) {
        int start = offset;
        int end = offset;
        int last = -1;
	Object jname = toJavaTextAttribute (name);
        try {
            if (accText != null) {
                javax.swing.text.AttributeSet attributes = 
                    accText.getCharacterAttribute(offset);
		if (!attributes.isDefined (jname)) {
		    defined.value = false;
		    return "";
		} 
		else {
		    defined.value = true;
		}
                last = accText.getCharCount ();

                javax.swing.text.AttributeSet tmp_attrs;
                start--;
                if ( start > 0 ) {
                    tmp_attrs = accText.getCharacterAttribute(start);
                    while ( start > 0 &&
                            (( attributes == null && tmp_attrs == null) ||
                             ( attributes != null && tmp_attrs != null &&
                               attributes.getAttribute (name) == tmp_attrs.getAttribute (jname) ) ) ) {
                        tmp_attrs = accText.getCharacterAttribute(--start);
                    }
                }

                end++;
                if ( end < last ) {
                    tmp_attrs = accText.getCharacterAttribute(end);
                    while ( end < last &&
                            (( attributes == null && attributes == tmp_attrs ) ||
                             ( attributes != null && tmp_attrs != null &&
                               attributes.getAttribute (name) == tmp_attrs.getAttribute (jname) ) ) ) {
                        tmp_attrs = accText.getCharacterAttribute(++end);
                    }
                }
                startOffset.value = ++start;
                endOffset.value = end;
                if ( attributes != null ) {
                    return fromJavaTextAttribute (jname, attributes.getAttribute (jname));
		}
                else
                    return "";
            }
        }
        catch ( Exception e ) {
	    defined.value = false;
            e.printStackTrace();
        }
        startOffset.value = offset;
        if ( last < 0 )
            endOffset.value = offset + 1;
        else 
            endOffset.value = last;
        return "";
	}

	public void unImplemented () {}
	public void unImplemented2 () {}
	public void unImplemented3 () {}
	public void unImplemented4 () {}

    private String typeToString( org.GNOME.Accessibility.TEXT_BOUNDARY_TYPE type ) {
        String rval;
        switch ( type.value() ) {
            case org.GNOME.Accessibility.TEXT_BOUNDARY_TYPE._TEXT_BOUNDARY_CHAR:
                rval = "_TEXT_BOUNDARY_CHAR";
                break;
            case org.GNOME.Accessibility.TEXT_BOUNDARY_TYPE._TEXT_BOUNDARY_WORD_START:
                rval = "_TEXT_BOUNDARY_WORD_START";
                break;
            case org.GNOME.Accessibility.TEXT_BOUNDARY_TYPE._TEXT_BOUNDARY_WORD_END:
                rval = "_TEXT_BOUNDARY_WORD_END";
                break;
            case org.GNOME.Accessibility.TEXT_BOUNDARY_TYPE._TEXT_BOUNDARY_SENTENCE_START:
                rval = "_TEXT_BOUNDARY_SENTENCE_START";
                break;
            case org.GNOME.Accessibility.TEXT_BOUNDARY_TYPE._TEXT_BOUNDARY_SENTENCE_END:
                rval = "_TEXT_BOUNDARY_SENTENCE_END";
                break;
            case org.GNOME.Accessibility.TEXT_BOUNDARY_TYPE._TEXT_BOUNDARY_LINE_START:
                rval = "_TEXT_BOUNDARY_LINE_START";
                break;
            case org.GNOME.Accessibility.TEXT_BOUNDARY_TYPE._TEXT_BOUNDARY_LINE_END:
                rval = "_TEXT_BOUNDARY_LINE_END";
                break;
            default:
                rval = "UNKNOWN!!!";
                break;
        }
        return rval;
    }
}
