/*
 * java-access-bridge for GNOME
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Accessibility;
import org.GNOME.Bonobo.*;
import javax.accessibility.*;

public class AccessibleFactory implements javax.naming.spi.ObjectFactory {

 static javax.naming.Name[] typeNames = new javax.naming.Name[3];
 static int ACCESSIBLE_TYPE = 0;
 static int TOPLEVEL_TYPE = 1;
 static int APPLICATION_TYPE = 2;
 static {
	 try { 
		 typeNames[ACCESSIBLE_TYPE] = 
			 new javax.naming.CompositeName("Accessible");
		 typeNames[TOPLEVEL_TYPE] = 
			 new javax.naming.CompositeName("Toplevel");
		 typeNames[APPLICATION_TYPE] = 
			 new javax.naming.CompositeName("Application");
	 } catch (Exception e) {
	 }
 };
 // TODO: make these more enum-like

	public Object getObjectInstance(Object ac_obj, javax.naming.Name name, javax.naming.Context ctx, java.util.Hashtable objectHashtable) {
		AccessibleContext ac = (AccessibleContext) ac_obj;
		Object object = objectHashtable.get(ac);
		if (object == null) {
			if ( name == typeNames[APPLICATION_TYPE] )
                object = new ApplicationImpl(ac);
            else 
                object = new AccessibleImpl(ac);
			((AccessibleImpl) object).tie().ref();
			objectHashtable.put(ac, object);
		} else {
			((AccessibleImpl) object).tie().ref();
		}
		return object;
	}

	public org.GNOME.Accessibility.Accessible 
	                      getAccessible (AccessibleContext ctx) {
	    org.GNOME.Accessibility.Accessible accessible = null;
	    try {
	        AccessibleImpl impl = (AccessibleImpl)
			JavaBridge.getAccessibleObjectFactory().
			getObjectInstance(ctx,
					  typeNames[ACCESSIBLE_TYPE],
					  null, 
					  JavaBridge.getAccessibleObjectTable());
		accessible = AccessibleHelper.narrow (impl.tie());
	    } catch (Exception ex) {
		    System.out.println ("Error getting accessible object: "+ex);
	    }
	    return accessible;
	}

	public org.GNOME.Accessibility.Application 
	                      getApplication (AccessibleContext ctx) {
	    org.GNOME.Accessibility.Application application = null;
	    try {
	        ApplicationImpl impl = (ApplicationImpl)
			JavaBridge.getAccessibleObjectFactory().
			getObjectInstance(ctx,
					  typeNames[APPLICATION_TYPE],
					  null, 
					  JavaBridge.getAccessibleObjectTable());
            application = ApplicationHelper.narrow (impl.tie());
	    } catch (Exception ex) {
		    System.out.println ("Error getting application object: "+ex);
	    }
	    return application;
	}
}
	
