/*
 * java-access-bridge for GNOME
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Accessibility;

import javax.accessibility.*;
import org.GNOME.Bonobo.*;

public class HypertextImpl extends UnknownImpl implements HypertextOperations {

	AccessibleContext   ac;

	public HypertextImpl ( AccessibleContext ac ) {
		super();
		this.ac = ac;
		this.poa = JavaBridge.getRootPOA();
		tie = new HypertextPOATie (this, JavaBridge.getRootPOA());
		try {
			JavaBridge.getRootPOA().activate_object(tie);
		} catch (Exception e) {
			System.out.println (e);
		}
	}

	private AccessibleHypertext getHypertext (AccessibleContext ac) {
		AccessibleText accText = ac.getAccessibleText();
		if (accText instanceof AccessibleHypertext)
			return (AccessibleHypertext) accText;
		else
			return null;
	}

	public int getNLinks () {
		int nLinks = 0;
		AccessibleHypertext accHypertext;

		accHypertext = getHypertext (ac);
		if(accHypertext != null )
			nLinks = accHypertext.getLinkCount();
		return nLinks;	
	}

	public org.GNOME.Accessibility.Hyperlink getLink (int linkIndex) {
		Hyperlink hyperlink = null;
		AccessibleHypertext accHypertext;

		accHypertext = getHypertext (ac);
		if (accHypertext != null) {
			javax.accessibility.AccessibleHyperlink hl = accHypertext.getLink(linkIndex);
			try {
				HyperlinkImpl hyperlinkImpl = 
					new HyperlinkImpl(hl);
				hyperlink = HyperlinkHelper.narrow (hyperlinkImpl.tie());
			} catch (Exception ex) {
				System.out.println ("Error getting link " + ex);
			}
		}
		return hyperlink;		
	}

	public int getLinkIndex (int characterIndex) {
		int linkIndex = 0;
		AccessibleHypertext accHypertext;

		accHypertext = getHypertext (ac);
		if(accHypertext != null )
			linkIndex = accHypertext.getLinkIndex(characterIndex);
		return linkIndex;		
	}

        public void unImplemented () {}
        public void unImplemented2 () {}
        public void unImplemented3 () {}
        public void unImplemented4 () {}
}
