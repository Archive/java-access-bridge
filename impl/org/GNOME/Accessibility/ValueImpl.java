/*
 * java-access-bridge for GNOME
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Accessibility;

import javax.accessibility.*;

public class ValueImpl extends org.GNOME.Bonobo.UnknownImpl implements ValueOperations {

	AccessibleValue accValue;

	public ValueImpl(AccessibleContext ac) {
		super();
	        accValue = ac.getAccessibleValue();
		this.poa = JavaBridge.getRootPOA();
		tie = new ValuePOATie (this, JavaBridge.getRootPOA());
		try {
			JavaBridge.getRootPOA().activate_object(tie);
		} catch (Exception e) {
			System.out.println (e);
		}
	}


	public double  minimumValue () {

		double minValue = 0.0D;

		if(accValue != null)
			minValue = accValue.getMinimumAccessibleValue().doubleValue();
		
		return minValue;	
  }
  
	public double maximumValue () {

		double maxValue = 0.0D;
		
		if(accValue != null)
			maxValue = accValue.getMaximumAccessibleValue().doubleValue();
		
		return maxValue;
		
	}

	public double minimumIncrement () {
		
		return 0.0D;
	}

	public double currentValue () {
		double currentValue = 0.0D;
		
		if(accValue != null)
			currentValue = accValue.getCurrentAccessibleValue().doubleValue();
		
		return currentValue;
		
	}
  
	public void currentValue (double newCurrentValue) {
		if(accValue != null) {
            // Workaround a problem in JDK where the setCurrentAccessibleValue
            // method only allows an Integer object to be used. Will be fixed
            // in later release, so first try passing a Double, if it fails
            // use Integer.
			if (accValue.setCurrentAccessibleValue( new Double(newCurrentValue) )==false){
                accValue.setCurrentAccessibleValue( new Integer ( (int)newCurrentValue ) );
            }
        }
	}
	
	public void unImplemented () {}
	public void unImplemented2 () {}	
	public void unImplemented3 () {}	
	public void unImplemented4 () {}	
}
