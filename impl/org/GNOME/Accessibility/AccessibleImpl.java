/*
 * java-access-bridge for GNOME 
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Accessibility;


import javax.accessibility.*;
import org.GNOME.Bonobo.*;

public class AccessibleImpl extends UnknownImpl implements AccessibleOperations {

  protected AccessibleContext ac;

  public AccessibleImpl(AccessibleContext ac) {
	  this ();
	  this.ac = ac;
	  this.ac.addPropertyChangeListener(JavaBridge.getGlobalPropertyChangeListener());
	  this.poa = JavaBridge.getRootPOA();
          tie = new AccessiblePOATie (this, JavaBridge.getRootPOA());
	  this.aggregate_interfaces ();
          try {
	    JavaBridge.getRootPOA().activate_object(tie);
	  } catch (Exception e) {
		  System.out.println (e);
	  }
  }

  public AccessibleImpl() {
	  super ();
  }

	// TODO: destructor: 
	// ac.removePropertyChangeListener(JavaBridge.getGlobalPropertyChangeListener());

  private void aggregate_interfaces () {
	  if (ac != null) {
          // System.err.println("aggregate_interfaces : ac = " + ac.toString());
		  UnknownImpl iface;
		  if (ac.getAccessibleAction() != null) {
              // System.err.println("aggregate_interfaces : ACTION");
			  iface = new ActionImpl (ac); 
			  this.add_interface (iface);
		  }
		  if (ac.getAccessibleComponent() != null) {
              // System.err.println("aggregate_interfaces : COMPONENT");
			  iface = new ComponentImpl (ac); 
			  this.add_interface (iface);
		  }
		  AccessibleText ac_text = ac.getAccessibleText();
		  if (ac_text != null) {
			  if (ac.getAccessibleEditableText() != null) {
              // System.err.println("aggregate_interfaces : EDITABLE_TEXT");
				  iface = new EditableTextImpl (ac); 
				  this.add_interface (iface);
			  }
              // System.err.println("aggregate_interfaces : TEXT ");
			  iface = new TextImpl (ac); 
			  this.add_interface (iface);
		          if (ac_text instanceof AccessibleHypertext) {
              // System.err.println("aggregate_interfaces : HYPER_TEXT");
				  iface = new HypertextImpl (ac); 
				  this.add_interface (iface);
			  }
		  }
		  if (ac.getAccessibleIcon() != null) {
              // System.err.println("aggregate_interfaces : ICON");
			  iface = new ImageImpl (ac); 
			  this.add_interface (iface);
		  }
		  if (ac.getAccessibleSelection() != null) {
              // System.err.println("aggregate_interfaces : SELECTION");
			  iface = new SelectionImpl (ac); 
			  this.add_interface (iface);
		  }
		  if (ac.getAccessibleTable() != null) {
              // System.err.println("aggregate_interfaces : TABLE");
			  iface = new TableImpl (ac); 
			  this.add_interface (iface);
		  }
		  if (ac.getAccessibleValue() != null) {
              // System.err.println("aggregate_interfaces : VALUE");
			  iface = new ValueImpl (ac); 
			  this.add_interface (iface);
		  }
	  }
  }

  public void name(String str) {

		if (ac != null)
			ac.setAccessibleName(str);
  }

  public String name() {
		String name = null;
		if (ac != null) {
			javax.accessibility.AccessibleRole javaRole =  ac.getAccessibleRole();

			// Return name of selected value as name of non-editable combo-box. 
			if (javaRole == AccessibleRole.COMBO_BOX &&
			    ac.getAccessibleChildrenCount() == 1) {
				AccessibleSelection sel = ac.getAccessibleSelection();
				if (sel != null) {
					java.lang.Object selected = sel.getAccessibleSelection(0);
					if (selected != null)
						name = ((javax.accessibility.Accessible) selected).getAccessibleContext().getAccessibleName();
				}
			}
		}
		if (name == null)
			name = ac.getAccessibleName();
		return (name != null) ? name : "";
  }

  public org.GNOME.Accessibility.Accessible parent() {

		org.GNOME.Accessibility.Accessible parent = null;
		if( ac != null ) {
			javax.accessibility.Accessible acc = ac.getAccessibleParent();
			if ( acc != null ) {
				parent = JavaBridge.getAccessibleObjectFactory().
					getAccessible(acc.getAccessibleContext());
			}
		}
		return parent;
  }

  public void description(String str) {

		if ( ac != null )
			ac.setAccessibleDescription(str);
  }
  
  public String description() {

		String desc = null;

		if ( ac != null) 
			desc = ac.getAccessibleDescription();
		return (desc != null) ? desc : "";
  }

  public int childCount(){

		int count = 0;
		if (ac != null)
			count = ac.getAccessibleChildrenCount();
		return count;
  }

  public boolean isEqual(org.GNOME.Accessibility.Accessible acc){
	  // TODO: test this!
	  if (ac == null || acc == null) return false;
	  return acc._is_equivalent (this.tie());
  }

  public org.GNOME.Accessibility.Accessible getChildAtIndex(int i) {

	  org.GNOME.Accessibility.Accessible child = null;
	  if ( ac != null  && (ac.getAccessibleChildrenCount() > i)) {
		  javax.accessibility.Accessible acc = ac.getAccessibleChild(i);
		  if (acc != null) {
			  child = JavaBridge.getAccessibleObjectFactory().
				  getAccessible(acc.getAccessibleContext());
		  } 
	  }
//	  if (child == null) 
//          System.out.println ("WARNING: null child returned\n");
	  return child;
  }

  public int getIndexInParent() {
	  int i = -1;
	  if( ac != null ) {
		  i = ac.getAccessibleIndexInParent();
	  }
	  return i;
  }

  public org.GNOME.Accessibility.Relation[] getRelationSet() {
	  Relation[] relation_set = {null};
	  if (ac != null) {
		  javax.accessibility.AccessibleRelationSet relationSet = 
			  ac.getAccessibleRelationSet();
          if ( relationSet != null ) {
              AccessibleRelation[] relations = relationSet.toArray();
              relation_set = new Relation[relations.length];
              for (int i=0; i<relations.length; ++i) {
                  RelationImpl impl = 
                      new RelationImpl (relations[i]);
                  Relation relation = null;
                  try {
                      relation = RelationHelper.narrow (impl.tie());
                  } catch (Exception ex) {
                      System.out.println ("Error getting relation:"+ex);
                  }
                  relation_set[i] = relation;
              }
          }
	  }
	  return relation_set;
  }

  public org.GNOME.Accessibility.Role getRole() {
	  Role role = Role.ROLE_INVALID;
	  if ( ac != null ) {
		  javax.accessibility.AccessibleRole javaRole =  ac.getAccessibleRole();
		  if (javaRole == AccessibleRole.ALERT)
			  role = Role.ROLE_ALERT;
		  else if (javaRole == AccessibleRole.AWT_COMPONENT)
			  role = Role.ROLE_UNKNOWN;
		  else if (javaRole == AccessibleRole.CANVAS)
			  role = Role.ROLE_CANVAS;
		  else if (javaRole == AccessibleRole.CHECK_BOX)
			  role = Role.ROLE_CHECK_BOX;
		  else if (javaRole == AccessibleRole.COLOR_CHOOSER)
			  role = Role.ROLE_COLOR_CHOOSER;
		  else if (javaRole == AccessibleRole.COLUMN_HEADER)
			  role = Role.ROLE_COLUMN_HEADER;
		  else if (javaRole == AccessibleRole.COMBO_BOX)
			  role = Role.ROLE_COMBO_BOX;
		  else if (javaRole == AccessibleRole.DATE_EDITOR)
			  role = Role.ROLE_DATE_EDITOR;
		  else if (javaRole == AccessibleRole.DESKTOP_ICON)
			  role = Role.ROLE_DESKTOP_ICON;
		  else if (javaRole == AccessibleRole.DESKTOP_PANE)
			  role = Role.ROLE_LAYERED_PANE;
		  else if (javaRole == AccessibleRole.DIALOG)
			  role = Role.ROLE_DIALOG;
		  else if (javaRole == AccessibleRole.DIRECTORY_PANE)
			  role = Role.ROLE_DIRECTORY_PANE;
		  else if (javaRole == AccessibleRole.FILE_CHOOSER)
			  role = Role.ROLE_FILE_CHOOSER;
		  else if (javaRole == AccessibleRole.FILLER)
			  role = Role.ROLE_FILLER;
		  else if (javaRole == AccessibleRole.FONT_CHOOSER)
			  role = Role.ROLE_FONT_CHOOSER;
		  else if (javaRole == AccessibleRole.FRAME)
			  role = Role.ROLE_FRAME;
		  else if (javaRole == AccessibleRole.GLASS_PANE)
			  role = Role.ROLE_GLASS_PANE;
		  else if (javaRole == AccessibleRole.GROUP_BOX)
			  role = Role.ROLE_PANEL;
		  else if (javaRole == AccessibleRole.HYPERLINK)
			  role = Role.ROLE_UNKNOWN;
		  else if (javaRole == AccessibleRole.ICON)
			  role = Role.ROLE_ICON;
		  else if (javaRole == AccessibleRole.INTERNAL_FRAME)
			  role = Role.ROLE_INTERNAL_FRAME;
		  else if (javaRole == AccessibleRole.LABEL)
			  role = Role.ROLE_LABEL;
		  else if (javaRole == AccessibleRole.LAYERED_PANE)
			  role = Role.ROLE_LAYERED_PANE;
		  else if (javaRole == AccessibleRole.LIST)
			  role = Role.ROLE_LIST;
		  else if (javaRole == AccessibleRole.LIST_ITEM)
			  role = Role.ROLE_LIST_ITEM;
		  else if (javaRole == AccessibleRole.MENU)
			  role = Role.ROLE_MENU;
		  else if (javaRole == AccessibleRole.MENU_BAR)
			  role = Role.ROLE_MENU_BAR;
		  else if (javaRole == AccessibleRole.MENU_ITEM)
			  role = Role.ROLE_MENU_ITEM;

		  else if (javaRole == AccessibleRole.OPTION_PANE)
			  role = Role.ROLE_OPTION_PANE;
		  else if (javaRole == AccessibleRole.PAGE_TAB)
			  role = Role.ROLE_PAGE_TAB;
		  else if (javaRole == AccessibleRole.PAGE_TAB_LIST)
			  role = Role.ROLE_PAGE_TAB_LIST;
		  else if (javaRole == AccessibleRole.PANEL)
			  role = Role.ROLE_PANEL;
		  else if (javaRole == AccessibleRole.PASSWORD_TEXT)
			  role = Role.ROLE_PASSWORD_TEXT;
		  else if (javaRole == AccessibleRole.POPUP_MENU)
			  role = Role.ROLE_POPUP_MENU;
		  else if (javaRole == AccessibleRole.PROGRESS_BAR)
			  role = Role.ROLE_PROGRESS_BAR;
		  else if (javaRole == AccessibleRole.PUSH_BUTTON)
			  role = Role.ROLE_PUSH_BUTTON;
		  else if (javaRole == AccessibleRole.RADIO_BUTTON) {
			  javax.accessibility.Accessible parent = ac.getAccessibleParent();
			  javax.accessibility.AccessibleRole parent_role = parent.getAccessibleContext().getAccessibleRole();

			  if (parent_role == AccessibleRole.MENU)
			      role = Role.ROLE_RADIO_MENU_ITEM;
			  else
			      role = Role.ROLE_RADIO_BUTTON;
		  }
		  else if (javaRole == AccessibleRole.ROOT_PANE)
			  role = Role.ROLE_ROOT_PANE;
		  else if (javaRole == AccessibleRole.ROW_HEADER)
			  role = Role.ROLE_ROW_HEADER;
		  else if (javaRole == AccessibleRole.SCROLL_BAR)
			  role = Role.ROLE_SCROLL_BAR;
		  else if (javaRole == AccessibleRole.SCROLL_PANE)
			  role = Role.ROLE_SCROLL_PANE;
		  else if (javaRole == AccessibleRole.SEPARATOR)
			  role = Role.ROLE_SEPARATOR;
		  else if (javaRole == AccessibleRole.SLIDER)
			  role = Role.ROLE_SLIDER;
		  else if (javaRole == AccessibleRole.SPIN_BOX)
			  role = Role.ROLE_SPIN_BUTTON;
		  else if (javaRole == AccessibleRole.SPLIT_PANE)
			  role = Role.ROLE_SPLIT_PANE;
		  else if (javaRole == AccessibleRole.STATUS_BAR)
			  role = Role.ROLE_STATUS_BAR;
		  else if (javaRole == AccessibleRole.SWING_COMPONENT)
			  role = Role.ROLE_UNKNOWN;
		  else if (javaRole == AccessibleRole.TABLE)
			  role = Role.ROLE_TABLE;
		  else if (javaRole == AccessibleRole.TEXT)
			  role = Role.ROLE_TEXT;
		  else if (javaRole == AccessibleRole.TOGGLE_BUTTON)
			  role = Role.ROLE_TOGGLE_BUTTON;
		  else if (javaRole == AccessibleRole.TOOL_BAR)
			  role = Role.ROLE_TOOL_BAR;
		  else if (javaRole == AccessibleRole.TOOL_TIP)
			  role = Role.ROLE_TOOL_TIP;
		  else if (javaRole == AccessibleRole.TREE)
			  role = Role.ROLE_TREE;
		  else if (javaRole == AccessibleRole.UNKNOWN) {
			  javax.accessibility.Accessible parent = ac.getAccessibleParent();
			  if (parent == null)
				role = Role.ROLE_APPLICATION;
			  else
			  	role = Role.ROLE_UNKNOWN;
		  }
		  else if (javaRole == AccessibleRole.VIEWPORT)
			  role = Role.ROLE_VIEWPORT;
		  else if (javaRole == AccessibleRole.WINDOW)
			  role = Role.ROLE_WINDOW;
		  else if (javaRole.toDisplayString (java.util.Locale.US).equalsIgnoreCase ("paragraph"))
		          role = Role.ROLE_PARAGRAPH;
		  else
			  role = Role.ROLE_EXTENDED;
	  }
      // System.err.println("ROLE: " + role.toString() );
	  return role;
  }

  public String getRoleName() {
	  // NOTE: this method currently returns a toolkit-specific role name
	  if ( ac != null ) {
		  AccessibleRole role =  ac.getAccessibleRole();
		  if (role == AccessibleRole.UNKNOWN) {
			  javax.accessibility.Accessible parent = ac.getAccessibleParent();
			  if (parent == null)
				return "application";
		  }
		  if (role != null) 
			  return role.toDisplayString(java.util.Locale.US);
	  } 
	  return "";
  }

  public org.GNOME.Accessibility.StateSet getState(){
	  StateSet state = null;
	  if (ac != null ) {
		  AccessibleStateSet ss = ac.getAccessibleStateSet();
		  AccessibleContext focus_ctx = JavaBridge.getFocusContext ();

		  if (ac == focus_ctx)
			ss.add (AccessibleState.FOCUSED);
		  StateSetImpl stateSetImpl = new StateSetImpl (ss);
		  state = StateSetHelper.narrow (stateSetImpl.tie());
	  }
	  return state;
  }

  public String getLocalizedRoleName() {
	  if ( ac != null ) {
		  AccessibleRole role =  ac.getAccessibleRole();
		  if (role == AccessibleRole.UNKNOWN) {
			  javax.accessibility.Accessible parent = ac.getAccessibleParent();
			  if (parent == null)
				return "application";
		  }
		  if (role != null) 
			  return role.toString();
	  } 
	  return "";
  }

  public String[] getAttributes () {
      return new String[0];
  }

  public Application getApplication () {
      // FIXME: need to return the real app object for this instance
      return null;
  }

  public void unimplemented () {}
}
