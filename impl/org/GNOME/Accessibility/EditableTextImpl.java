/*
 * java-access-bridge for GNOME
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Accessibility;
import org.GNOME.Bonobo.*;
import javax.accessibility.*;
import java.awt.datatransfer.*;
import java.awt.Toolkit;
import java.awt.Color;
import java.util.StringTokenizer;
import javax.swing.text.*;

public class EditableTextImpl extends TextImpl implements EditableTextOperations {

	public EditableTextImpl (AccessibleContext ac) {
		super (ac);

		tie = new EditableTextPOATie (this,
					      JavaBridge.getRootPOA());
		try {
			JavaBridge.getRootPOA().activate_object(tie);
		} catch (Exception e) {
			System.out.println (e);
		}
	}

	public boolean setTextContents (String newContents) {

		if (accEditableText != null) {
			accEditableText.setTextContents (newContents);
			return true;
		}
		return false;
	}

	public boolean insertText (int position, String text, int length) {
        if ( accEditableText != null && text != null ) {
            if ( position < 0 )
                position = 0;

            accEditableText.insertTextAtIndex( position, text );

            return true;
        }

        return false ;
	}

    // Assumes that the attributes string contains CSS attribute names
    // and values in the format:
    //      <attrName>:<attrValue>[, <attrName>:<attrValue>]...
    //
    // Currently supported attributes are:
    // 
    private AttributeSet convertStringToAttributeSet( String attributes ) {
        SimpleAttributeSet aSet = new SimpleAttributeSet();

        String workStr = attributes.replaceAll( ",\\s+", "|" );
        StringTokenizer elements = new StringTokenizer( workStr, "|" );
        while ( elements.hasMoreElements() ) {
            String element = elements.nextToken();
            StringTokenizer attr = new StringTokenizer( element, ": " );
            if ( attr.countTokens() == 2 ) {
                String name = attr.nextToken();
                String value= attr.nextToken();
                
                // So what object is "name"
                if ( name.equals( "family-name" ) ) {
                    StyleConstants.setFontFamily( aSet, value );
                }
                else if ( name.equals( "size" ) ) {
                    // Check if value is an int
                    try {
                        StyleConstants.setFontSize( aSet, Integer.parseInt( value ) );
                    }
                    catch ( NumberFormatException e ) {
                    // TODO - For now, do nothing, not clear how to
                    //        handle strings like "smaller" - smaller than
                    //        what?
                    }
                }
                else if ( name.equals( "style" ) ) {
                    if ( ( value.equals( "italic" ) ) || 
                         ( value.equals( "oblique" ) ) ) {
                        StyleConstants.setItalic( aSet, true );
                    }
                    else    // value == "normal"
                        StyleConstants.setItalic( aSet, false );
                }
                else if ( name.equals( "weight" ) ) {
                    try {
                        // First see if it's a number.
                        // Values are based on CSS weights
                        int weight = Integer.parseInt( value );
                        switch ( weight ) {
                            case 500: // Medium
                            case 600: // semi/demi-bold
                            case 700: // Bold
                            case 800: // Extra/Ultra-bold
                            case 900: // Heavy / Black
                                StyleConstants.setBold( aSet, true );
                                break;
                            
                            case 100:
                            case 200:
                            case 300: // Light
                            case 400: // Normal
                            default: 
                                StyleConstants.setBold( aSet, false );
                                break;
                        }
                    }
                    catch ( NumberFormatException e ) {
                        if ( value.equals( "medium" ) ||    
                             value.equals( "demibold" ) ||
                             value.equals( "semibold" ) ||
                             value.equals( "bold" ) ||
                             value.equals( "extrabold" ) ||
                             value.equals( "ultrabold" ) ||
                             value.equals( "heavy" ) ||
                             value.equals( "black" ) ) {
                            StyleConstants.setBold( aSet, true );
                        }
                    }
                    // TODO - Not clear how to handle strings like "lighter" 
                    //        i.e. lighter than what?
                }
                else if ( name.equals( "fg-color" ) ) {
                    try {
                        // try decode string - eg. #fffe00
                        StyleConstants.setForeground( aSet, 
                                        new Color ( Integer.decode(value).intValue() ) );
                    }
                    catch ( NumberFormatException e ) {
                        // Otherwise, try format 65535,65535,65535
                        StringTokenizer nums = new StringTokenizer( value, "," );
                        if ( nums.countTokens() == 3 ) {
                            try {
                                int red   = Integer.parseInt(nums.nextToken()) / 256;
                                int green = Integer.parseInt(nums.nextToken()) / 256;
                                int blue  = Integer.parseInt(nums.nextToken()) / 256;

                                StyleConstants.setForeground( aSet, 
                                                new Color ( red, green, blue ) );
                            }
                            catch ( NumberFormatException e2 ) {
                            }
                        }
                    }
                }
                else if ( name.equals( "bg-color" ) ) {
                    try {
                        // try decode string - eg. #fffe00
                        StyleConstants.setBackground( aSet, 
                                        new Color ( Integer.decode(value).intValue() ) );
                    }
                    catch ( NumberFormatException e ) {
                        // Otherwise, try format 65535,65535,65535
                        StringTokenizer nums = new StringTokenizer( value, "," );
                        if ( nums.countTokens() == 3 ) {
                            try {
                                int red   = Integer.parseInt(nums.nextToken()) / 256;
                                int green = Integer.parseInt(nums.nextToken()) / 256;
                                int blue  = Integer.parseInt(nums.nextToken()) / 256;

                                StyleConstants.setBackground( aSet, 
                                                new Color ( red, green, blue ) );
                            }
                            catch ( NumberFormatException e2 ) {
                            }
                        }
                    }
                }
                else if ( name.equals( "underline" ) ) {
                    if ( value.equals( "none" ) ) 
                        StyleConstants.setUnderline( aSet, false );
                    else
                        StyleConstants.setUnderline( aSet, true );
                }
                else if ( name.equals( "strikethrough" ) ) {
                    StyleConstants.setStrikeThrough( aSet, 
                                            Boolean.valueOf( value ).booleanValue() );
                }
                else {  // Default to Strings
                    aSet.addAttribute( name, value );
                }
                
            } 
        }
        return aSet;
    }

	public boolean setAttributes (String attributes, int startPos, int endPos) {
        if ( accEditableText != null ) {
            int nchars = accEditableText.getCharCount();
            if ( startPos < 0 )
                startPos = 0;
            if (endPos >= nchars || endPos == -1) 
                endPos = nchars;
            else if ( endPos < -1 )
                endPos = 0;

            AttributeSet aSet = convertStringToAttributeSet( attributes );
            if ( aSet != null ) {
                accEditableText.setAttributes( startPos, endPos, aSet );
                return true;
            }
        }

		return false;
	}

	public void copyText (int startPos, int endPos) {
        if ( accEditableText != null ) {
            int nchars = accEditableText.getCharCount();
            if ( startPos < 0 )
                startPos = 0;
            if (endPos >= nchars || endPos == -1) 
                endPos = nchars;
            else if ( endPos < -1 )
                endPos = 0;

            String string = accEditableText.getTextRange( startPos, endPos );
            if ( string != null ) {
                StringSelection string_selection = new StringSelection (string);
                Toolkit.getDefaultToolkit().getSystemClipboard().setContents(
                                            string_selection, string_selection);
            }
        }
	}

	public boolean cutText (int startPos, int endPos) {
        if ( accEditableText != null ) {
            int nchars = accEditableText.getCharCount();
            if ( startPos < 0 )
                startPos = 0;
            if (endPos >= nchars || endPos == -1) 
                endPos = nchars;
            else if ( endPos < -1 )
                endPos = 0;

            accEditableText.cut( startPos, endPos );
            return true;
        }

        return false;
	}

	public boolean deleteText (int startPos, int endPos) {
        if ( accEditableText != null ) {
            int nchars = accEditableText.getCharCount();
            if ( startPos < 0 )
                startPos = 0;
            if (endPos >= nchars || endPos == -1) 
                endPos = nchars;
            else if ( endPos < -1 )
                endPos = 0;

            accEditableText.delete( startPos, endPos );
            return true;
        }

        return false;
	}

	public boolean pasteText (int position) {
        if ( accEditableText != null ) {
            if ( position < 0 )
                position = 0;

            accEditableText.paste( position );
            return true;
        }

        return false;
	}

	public void unImplemented5 () {}
	public void unImplemented6 () {}
	public void unImplemented9 () {}
	public void unImplemented10 () {}
	public void unImplemented11 () {}
	public void unImplemented12 () {}
}

