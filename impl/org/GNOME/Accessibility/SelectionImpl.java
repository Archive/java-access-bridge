/*
 * java-access-bridge for GNOME
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Accessibility;
import javax.accessibility.*;
import org.GNOME.Bonobo.*;

public class SelectionImpl extends UnknownImpl implements SelectionOperations {


	AccessibleSelection accSelection;
	AccessibleContext ac;

	public SelectionImpl(javax.accessibility.AccessibleContext ac) {
		super();	
		accSelection = ac.getAccessibleSelection();
		this.ac = ac;
		this.poa = JavaBridge.getRootPOA();
		tie = new SelectionPOATie (this, JavaBridge.getRootPOA());
		try {
			JavaBridge.getRootPOA().activate_object(tie);
		} catch (Exception e) {
			System.out.println (e);
		}
	}

	public int nSelectedChildren () {		
		int count = 0;
		if(accSelection != null)
			count = accSelection.getAccessibleSelectionCount();
		return count;			
	}


	public Accessible getSelectedChild (int selectedChildIndex) {
		Accessible selection = null;
		if( accSelection != null) {
		    javax.accessibility.Accessible acc = 
			accSelection.getAccessibleSelection(selectedChildIndex);
		    selection = JavaBridge.getAccessibleObjectFactory().
			    getAccessible(acc.getAccessibleContext());
		}
		return selection;
	}

	public boolean selectChild (int childIndex) {
		boolean selected = false;
		if(accSelection != null) {
			accSelection.addAccessibleSelection(childIndex);
			selected = isChildSelected(childIndex);
		}
		return selected;			
	}

	public boolean deselectSelectedChild (int selectedChildIndex) {
		boolean deselected = false;
		if(accSelection != null) {
			accSelection.removeAccessibleSelection(selectedChildIndex);
			deselected = !isChildSelected(selectedChildIndex);
		}
		return deselected;
	}
 
	public boolean deselectChild (int childIndex) {
	    // TODO: FIXME Implement this!
	        return false;
	}

	public boolean isChildSelected (int childIndex) {
		if(accSelection != null)
			return accSelection.isAccessibleChildSelected(childIndex);
		return false;
	}

	public boolean selectAll () {
		if (accSelection != null) {
			AccessibleStateSet ss = ac.getAccessibleStateSet ();

			if (ss.contains (AccessibleState.MULTISELECTABLE)) {
				accSelection.selectAllAccessibleSelection();
				return true;
			}
		}
		return false;
	}

	public boolean clearSelection () {
		if(accSelection != null) {
			accSelection.clearAccessibleSelection();
			return true;
		}
		else
			return false;
	}

	public void unImplemented () {}
	public void unImplemented2 () {}
	public void unImplemented3 () {}
	public void unImplemented4 () {}
}

