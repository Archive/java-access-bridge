/*
 * java-access-bridge for GNOME
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Accessibility;
import org.GNOME.Bonobo.*;

import javax.accessibility.*;
public class ImageImpl extends UnknownImpl implements ImageOperations {

	private AccessibleIcon[] accIcons;
	private AccessibleContext ac;
	
	public ImageImpl(AccessibleContext ac) {
		super();
		this.ac = ac;
		accIcons = ac.getAccessibleIcon();
		this.poa = JavaBridge.getRootPOA();
		tie = new ImagePOATie (this, JavaBridge.getRootPOA());
		try {
			JavaBridge.getRootPOA().activate_object(tie);
		} catch (Exception e) {
			System.out.println (e);
		}
	}

	public String imageDescription () {
		String desc = "";
		if (accIcons != null && accIcons.length > 0 ) {
			desc = accIcons[0].getAccessibleIconDescription ();
            if ( desc == null ) 
                desc = "";
		}
		return desc;
	}

        public org.GNOME.Accessibility.BoundingBox getImageExtents (short coordType) {
		org.GNOME.Accessibility.BoundingBox bbox = 
			new org.GNOME.Accessibility.BoundingBox ();
		if (accIcons != null) {
			java.awt.Point pos = 
				ac.getAccessibleComponent ().getLocationOnScreen();
			bbox.x = pos.x;
			bbox.y = pos.y;
			bbox.width = accIcons[0].getAccessibleIconWidth ();
			bbox.height = accIcons[0].getAccessibleIconHeight ();
		} 
		return bbox;
	}

	public void getImagePosition (org.omg.CORBA.IntHolder x, 
				      org.omg.CORBA.IntHolder y, 
				      short coordType) {
		if (ac != null) {
			java.awt.Point p = 
				ac.getAccessibleComponent ().getLocationOnScreen (); 
            if ( p != null ) {
                x.value = p.x;
                y.value = p.y;
            }
		}
	}

	public void getImageSize (org.omg.CORBA.IntHolder width, 
				  org.omg.CORBA.IntHolder height) {
		if (accIcons != null) {
			width.value = accIcons[0].getAccessibleIconWidth ();
			height.value = accIcons[0].getAccessibleIconHeight ();
		}
	}

        public String imageLocale () {
	    // FIXME
	    return null;
	}
	public void unImplemented () {}
	public void unImplemented2 () {}
	public void unImplemented3 () {}

}
