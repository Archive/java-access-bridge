/*
 * java-access-bridge for GNOME
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Accessibility;
import org.GNOME.Bonobo.*;

import javax.accessibility.*;
public class RelationImpl extends UnknownImpl implements RelationOperations {

	private javax.accessibility.AccessibleRelation accRelation;
	
	public RelationImpl(javax.accessibility.AccessibleRelation relation) {
		super();
		this.accRelation = relation;
		this.poa = JavaBridge.getRootPOA();
		tie = new RelationPOATie (this, JavaBridge.getRootPOA());
		try {
			JavaBridge.getRootPOA().activate_object(tie);
		} catch (Exception e) {
			System.out.println (e);
		}
	}

	public RelationType getRelationType () {
		RelationType type = RelationType.RELATION_NULL;
		if (accRelation != null) {
 		    if (accRelation.getKey() == AccessibleRelation.LABEL_FOR)
			    type = RelationType.RELATION_LABEL_FOR;
		    else if (accRelation.getKey() == AccessibleRelation.LABELED_BY)
			    type = RelationType.RELATION_LABELLED_BY;
		    else if (accRelation.getKey() == AccessibleRelation.CONTROLLER_FOR)
			    type = RelationType.RELATION_CONTROLLER_FOR;
		    else if (accRelation.getKey() == AccessibleRelation.CONTROLLED_BY)
			    type = RelationType.RELATION_CONTROLLED_BY;
		    else if (accRelation.getKey() == AccessibleRelation.MEMBER_OF)
			    type = RelationType.RELATION_MEMBER_OF;
		    else if (accRelation.getKey().equals ("flowsFrom"))
			    type = RelationType.RELATION_FLOWS_FROM;
		    else if (accRelation.getKey().equals ("flowsTo"))
			    type = RelationType.RELATION_FLOWS_TO;
		}
		return type;
	}

	public String getRelationTypeName () {
		String s = null;
		if (accRelation != null) 
			s = accRelation.getKey();
		return s;
	}

	public short getNTargets () {
		short n = 0;
		if (accRelation != null) 
			n = (short) accRelation.getTarget().length;
		return n;
	}

	public org.omg.CORBA.Object getTarget (short index) {
		org.omg.CORBA.Object o =  null;
		if (accRelation != null) {
			java.lang.Object targets[] = accRelation.getTarget();
			java.lang.Object obj = targets[index];
			if (obj instanceof javax.accessibility.Accessible) {
				try {
				UnknownImpl impl = (UnknownImpl)
					JavaBridge.getAccessibleObjectFactory().
					getObjectInstance(
					((javax.accessibility.Accessible) obj).
					getAccessibleContext(),
					AccessibleFactory.typeNames[
						AccessibleFactory.ACCESSIBLE_TYPE],
					null,
					JavaBridge.getAccessibleObjectTable());
				o = impl.tie();
				} catch (Exception ex) {
					System.out.println ("Error getting target: "
							    +ex);
				}
			} 
		}
		return o;
	}
	
	/* placeholders for future expansion */
	public void unImplemented () {}
	public void unImplemented2 () {}
	public void unImplemented3 () {}
	public void unImplemented4 () {}
}
