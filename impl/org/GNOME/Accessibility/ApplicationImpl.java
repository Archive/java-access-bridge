/*
 * java-access-bridge for GNOME
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Accessibility;

import javax.accessibility.*;

public class ApplicationImpl  extends AccessibleImpl implements ApplicationOperations {

  private int id;

  public org.GNOME.Accessibility.Role getRole() {
	  return Role.ROLE_APPLICATION;
  }

  public String toolkitName (){

		return "J2SE-access-bridge";
  }

  public String version (){

		return "0.1";
  }

  public int id (){
		return this.id;
  }

  public  void id (int newId){

		this.id = newId;
	
  }

  public void registerToolkitEventListener (org.GNOME.Accessibility.EventListener listener, String eventName){


  }

	public void registerObjectEventListener (org.GNOME.Accessibility.EventListener listener, String eventName) {

	}
  public boolean pause (){

		return true;
  }

 public boolean resume (){

		return true;
 }

 public String getLocale (org.GNOME.Accessibility.LOCALE_TYPE lctype) {
                return null;
 }
 
 public ApplicationImpl(AccessibleContext ac) {
	 super ();
	 this.ac = ac;
	 this.poa = JavaBridge.getRootPOA();
	 tie = new ApplicationPOATie (this, JavaBridge.getRootPOA());
	 try {
		 JavaBridge.getRootPOA().activate_object(tie);
	 } catch (Exception e) {
		 System.out.println (e);
	 }
  }

  public void unImplemented_ () {}
  public void unImplemented2_ () {}
  public void unImplemented3_ () {}

}
