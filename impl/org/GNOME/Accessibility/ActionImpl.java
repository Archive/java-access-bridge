/*
 * java-access-bridge for GNOME
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Accessibility;
import org.GNOME.Bonobo.*;

import javax.accessibility.*;
import javax.swing.*;
import java.awt.event.KeyEvent;

public class ActionImpl extends UnknownImpl implements ActionOperations {

	private AccessibleAction accAction;
	private AccessibleExtendedComponent accExComponent;
	
	public ActionImpl(AccessibleContext ac) {
		super();
	        accAction = ac.getAccessibleAction();
	        if (ac.getAccessibleComponent() instanceof 
		    AccessibleExtendedComponent)
			accExComponent = (AccessibleExtendedComponent) 
				ac.getAccessibleComponent();
		this.poa = JavaBridge.getRootPOA();
		tie = new ActionPOATie (this, JavaBridge.getRootPOA());
		try {
			JavaBridge.getRootPOA().activate_object(tie);
		} catch (Exception e) {
			System.err.println (e);
		}
	}

	public ActionImpl(AccessibleHyperlink hl) {
		super();
	        accAction = hl;
		this.poa = JavaBridge.getRootPOA();
		tie = new ActionPOATie (this, JavaBridge.getRootPOA());
		try {
			JavaBridge.getRootPOA().activate_object(tie);
		} catch (Exception e) {
			System.err.println (e);
		}
	}

	public int nActions ()
	{
		int count = 0;

		if(accAction != null)
			count = accAction.getAccessibleActionCount();

		return count;
	}

	public String getDescription (int index) {
			String description  = "<description>";
			return  description;
	}
	
	public String getName (int index) {
		String name;
		if (accAction != null) {
			name = accAction.getAccessibleActionDescription (index);
			if (name != null) return name;
		}
		return " ";	
	}

    private class ActionRunner implements Runnable {
        private AccessibleAction accAction;
        private int              index;

        public ActionRunner( AccessibleAction _accAction, int _index ) {
            accAction = _accAction;
            index = _index;
        }

        public void run() {
            accAction.doAccessibleAction(index);
        }
    }

	public boolean doAction (int index) {
		// System.err.println("doAction( "+index+" )");
		try {
			if(accAction != null) {
                SwingUtilities.invokeLater( new ActionRunner( accAction, index ));
                return true;
			}
		} catch (Exception e ) {
			System.err.println("Exception : " + e.toString() );
		}
		return false;
	}

    private String convertModString( String mods ) {
        if ( mods == null || mods.length() == 0 )
            return "";

        String modStrs[] = mods.split("\\+");
        String newModString = "";
        for ( int i = 0; i < modStrs.length; i++ )
            newModString += "<" + modStrs[i] + ">";

        return newModString;
    }

	public String getKeyBinding (int index) {
		// TODO: improve/fix conversion to strings, concatenate,
		//       and follow our formatting convention for the role of
		//       various keybindings (i.e. global, transient, etc.)
        //
        // Presently, JAA doesn't define a relationship between the index used
        // and the action associated. As such, all keybindings are only
        // associated with the default (index 0 in GNOME) action.
        //
        if ( index > 0 ) 
            return "";

		if(accExComponent != null) {
			AccessibleKeyBinding akb = 
				accExComponent.getAccessibleKeyBinding();
			if (akb != null && akb.getAccessibleKeyBindingCount() > 0 ) {
                String  rVal = "";
                int     i;

                // Privately Agreed interface with StarOffice to workaround
                // deficiency in JAA.
                // 
                // The aim is to use an array of keystrokes, if there is more
                // than one keypress involved meaning that we would have:
                //
                //      KeyBinding(0)    -> nmeumonic       KeyStroke
                //      KeyBinding(1)    -> full key path   KeyStroke[]
                //      KeyBinding(2)    -> accelerator     KeyStroke
                //
                // GNOME Expects a string in the format:
                //
                //      <nmemonic>;<full-path>;<accelerator>
                //
		// The keybindings in <full-path> should be separated by ":"
		//
                // Since only the first three are relevant, ignore others
                for (i = 0;( i < akb.getAccessibleKeyBindingCount() && i < 3); i++){
                    Object o = akb.getAccessibleKeyBinding(i); 

                    if ( i > 0 )
                        rVal += ";";

                    if (o instanceof javax.swing.KeyStroke) { 
                        javax.swing.KeyStroke keyStroke = (javax.swing.KeyStroke)o;  
                        String modString = KeyEvent.getKeyModifiersText(keyStroke.getModifiers());
                        String keyString = KeyEvent.getKeyText(keyStroke.getKeyCode());

                        if ( keyString != null ) {
                            if ( modString != null && modString.length() > 0 )
                                rVal += convertModString(modString) + keyString;
                            else
                                rVal += keyString;
                        }

                    } 
                    else if (o instanceof javax.swing.KeyStroke[]) { 
                        javax.swing.KeyStroke[] keyStroke = (javax.swing.KeyStroke[])o;  
                        for ( int j = 0; j < keyStroke.length; j++ ) {
                            String modString = KeyEvent.getKeyModifiersText(keyStroke[j].getModifiers());
                            String keyString = KeyEvent.getKeyText(keyStroke[j].getKeyCode());

			    if (j > 0)
				rVal += ":";
                            if ( keyString != null ) {
                                if ( modString != null && modString.length() > 0 )
                                    rVal += convertModString(modString) + keyString;
                                else
                                    rVal += keyString;
                            }
                        }
                    }
                    // else
                        // System.err.println("keybinding returned null");
                }
                if ( i < 2 ) rVal += ";";
                if ( i < 3 ) rVal += ";";
                return rVal;
			}
            // else
                // System.err.println("akb is null");
		}		
        // else
            // System.err.println("accExComponent is null");
		return "";
	}

	public void unImplemented() {}
	public void unImplemented2() {}
	public void unImplemented3() {}
	public void unImplemented4() {}
}
