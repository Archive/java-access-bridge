/*
 * java-access-bridge for GNOME
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Accessibility;
import org.GNOME.Bonobo.*;
import javax.accessibility.AccessibleTable;
import javax.accessibility.AccessibleExtendedTable;

public class TableImpl extends UnknownImpl implements TableOperations {

	AccessibleTable accTable = null;

	public TableImpl ( javax.accessibility.AccessibleContext ac ) {
		super();
		accTable = ac.getAccessibleTable();
		this.poa = JavaBridge.getRootPOA();
		tie = new TablePOATie (this, JavaBridge.getRootPOA());
		try {
			JavaBridge.getRootPOA().activate_object(tie);
		} catch (Exception e) {
			System.out.println (e);
		}
	}

	public int nRows () {
		int rows = 0;
		if (accTable != null )
			rows = accTable.getAccessibleRowCount();
		return rows;
	}
  
	public int nColumns () {
		int cols = 0;
		if (accTable != null )
			cols = accTable.getAccessibleColumnCount();
		return cols;
	}

        public Accessible caption () {
		Accessible caption = null;
		if (accTable != null ) {
		    AccessibleImpl captionImpl;
		    javax.accessibility.Accessible acc = 
			    accTable.getAccessibleCaption();
            if ( acc != null ) {
                caption = JavaBridge.getAccessibleObjectFactory().
                    getAccessible (acc.getAccessibleContext());
            }
		}
		return caption;
	}

	public Accessible summary () {

		Accessible summary = null;
		if (accTable != null ) {
			javax.accessibility.Accessible acc = 
				accTable.getAccessibleSummary();
			if (acc != null)
				summary = JavaBridge.getAccessibleObjectFactory().
					getAccessible (acc.getAccessibleContext());
		}
		return summary;
	}

	public int nSelectedRows () {
		int nSelected = 0;
		if (accTable != null ) { 
			int[] selectedRows = accTable.getSelectedAccessibleRows();
			nSelected = selectedRows.length;	
		}
		return nSelected;
	}

	public int nSelectedColumns () {
		int nSelected = 0;
		if (accTable != null ) { 
			int[] selectedCols = accTable.getSelectedAccessibleColumns();
			nSelected = selectedCols.length;	
		}
		return nSelected;
	}

	public Accessible getAccessibleAt (int row, int column) {
		Accessible accAt = null;
		if (accTable != null ) {
			javax.accessibility.Accessible acc = accTable.getAccessibleAt(row, column);
			if (acc != null) {
				accAt = JavaBridge.getAccessibleObjectFactory().
					getAccessible (acc.getAccessibleContext());
			}
		}
		return accAt;
	}

    public int getIndexAt (int row, int column) {
  	  int index = -1;
  	  if (accTable instanceof AccessibleExtendedTable ) {
  	    index =
  	    ((AccessibleExtendedTable) accTable).getAccessibleIndex (row, column);
  	  }
  	  return index;
    }
  
    // XXX - There is a bug in the JVM that returns a value of:
    //      
    //       (ColumnCount() * RowCount() ) / RowCount()
    //
    //       Which is equal to ColumnCount() naturally...
    // 
    // Notice how it doesn't even consider the index!
    //
    // This is true for getAccessbileRow() and getAccessbileColumn()
    //
    private boolean validGetXXAtIndexImpl = true;
    private boolean testIndexingFirstTime = true;

    private boolean getXXAtIndexValid() {

        if ( testIndexingFirstTime ) {
            if ( accTable != null && accTable instanceof AccessibleExtendedTable) {
                AccessibleExtendedTable extTable = (AccessibleExtendedTable) accTable;
                validGetXXAtIndexImpl = (accTable.getAccessibleColumnCount() != 
                                         extTable.getAccessibleRow(0));
            }
            testIndexingFirstTime = false;
        }

        return (validGetXXAtIndexImpl);
    }

    public int getRowAtIndex (int index) {
        int row = -1;
        if (getXXAtIndexValid() && accTable instanceof AccessibleExtendedTable) {
             row = ((AccessibleExtendedTable) accTable).getAccessibleRow(index);
        }
        else {
            int numCells = accTable.getAccessibleColumnCount() 
                           * accTable.getAccessibleRowCount();
            if (index >= numCells) {
                return -1;
            }
            else {
                row = index / accTable.getAccessibleColumnCount();
            }
        }
        System.err.println("getRowAtIndex: returning : " + row );
        return row;
    }
  
    public int getColumnAtIndex (int index) {
        int column = -1;
        if (getXXAtIndexValid() && accTable instanceof AccessibleExtendedTable) {
           column = ((AccessibleExtendedTable) accTable).getAccessibleColumn(index);
  	    }
        else {
            int numCells = accTable.getAccessibleColumnCount() 
                           * accTable.getAccessibleRowCount();
            if (index >= numCells) {
                return -1;
            }
            else {
                column = index % accTable.getAccessibleColumnCount();
            }
        }
        return column;
    }

	public String getRowDescription (int row) {
		String rowDescription = "";
		if (accTable != null ) {
			javax.accessibility.Accessible acc = 
				accTable.getAccessibleRowDescription(row);
			if ( acc != null ) {
				rowDescription = 
					acc.getAccessibleContext().getAccessibleDescription();
			}
		}
		return rowDescription;
	}

	public String getColumnDescription (int column) {
		String colDescription = "";
		if (accTable != null ) {
			javax.accessibility.Accessible acc = 
				accTable.getAccessibleColumnDescription(column);
			if ( acc != null ) {
				colDescription = 
					acc.getAccessibleContext().getAccessibleDescription();
			}
		}
		return colDescription;
	}

	public int getRowExtentAt (int row, int column) {
		int extent = 0;
		if (accTable != null ) 
			extent = accTable.getAccessibleRowExtentAt(row, column);
		return extent;
	}

	public int getColumnExtentAt (int row, int column) {
		int extent = 0;
		if (accTable != null ) 
			extent = accTable.getAccessibleColumnExtentAt(row, column);
		return extent;
	}

	public Accessible getRowHeader (int row) {
		Accessible rowAccessible = null;
		if ( accTable != null ) {
			AccessibleTable rowHeaderTable = 
				accTable.getAccessibleRowHeader();
			if (rowHeaderTable != null) {
				javax.accessibility.Accessible accessible = 
					rowHeaderTable.getAccessibleAt (row, 0);
				if (accessible != null) {
					javax.accessibility.AccessibleContext rac =
						accessible.getAccessibleContext ();
					rowAccessible = 
					JavaBridge.getAccessibleObjectFactory().
						getAccessible (rac);
				}
			}
		}		
		return rowAccessible; 
	}

	public Accessible getColumnHeader (int column) {
		Accessible colAccessible = null;
		if ( accTable != null ) {
			AccessibleTable colHeaderTable = 
				accTable.getAccessibleColumnHeader();
			if (colHeaderTable != null)
			{
				javax.accessibility.Accessible accessible = 
					colHeaderTable.getAccessibleAt (0, column);
				if (accessible != null) {
					javax.accessibility.AccessibleContext cac =
						accessible.getAccessibleContext ();
					colAccessible = 
						JavaBridge.getAccessibleObjectFactory().
						getAccessible (cac);
				} 
			}
		}
		return colAccessible;
	}

        public boolean getRowColumnExtentsAtIndex(int p1,org.omg.CORBA.IntHolder p2,org.omg.CORBA.IntHolder p3,org.omg.CORBA.IntHolder p4,org.omg.CORBA.IntHolder p5,org.omg.CORBA.BooleanHolder p6) {
                return false;
        }

	public int[] getSelectedRows () {

		int[] selectedRows = null;
		if (accTable != null )
			selectedRows = accTable.getSelectedAccessibleRows();
		return selectedRows; 
	}

	public int[] getSelectedColumns () {
		
		int[] selectedCols = null;
		if (accTable != null )
			selectedCols = accTable.getSelectedAccessibleColumns();
		return selectedCols; 
	}

	public boolean isRowSelected (int row) {
		
		boolean selected = false;
		if ( accTable != null)
			selected = accTable.isAccessibleRowSelected( row );
		return selected;
	}
	
	public boolean isColumnSelected (int column) {
		
		boolean selected = false;
		if ( accTable != null)
			selected = accTable.isAccessibleColumnSelected( column );
		return selected;
	}
	
	public boolean isSelected (int row, int column) {
		
		boolean selected = false;
		if ( accTable != null)
			selected = accTable.isAccessibleSelected( row, column );
		return selected;
	}

	// JA-API Table Selection API is read-only !

	public boolean addRowSelection (int row) {
		return false; 
	}

	public boolean addColumnSelection (int column) {
		return false; 
	}

	public boolean removeRowSelection (int row) {
		return false; 
	}

	public boolean removeColumnSelection (int column) {
		return false; 
	}

	public void unImplemented () {}
	public void unImplemented2 () {}
	public void unImplemented3 () {}
	public void unImplemented4 () {}
	public void unImplemented5 () {}
	public void unImplemented6 () {}
	public void unImplemented7 () {}
	public void unImplemented8 () {}
}
