/*
 * java-access-bridge for GNOME 
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Accessibility;

import javax.accessibility.*;
import org.GNOME.Bonobo.*;
import java.util.*;

public class RegistryImpl extends UnknownImpl implements RegistryOperations {

	private Desktop[] desktops;
	private DeviceEventController deviceEventController;
	private HashSet applications = new LinkedHashSet ();

	public void notifyEvent (Event e) {
		// TODO: implement
	}

	public void registerApplication (Application application) {
		applications.add (application);
	}

	public void deregisterApplication (Application application) {
		applications.remove (application);
	}

	public void registerGlobalEventListener (
		org.GNOME.Accessibility.EventListener listener,
		String eventName) {
		// TODO: implement
	}

	public void deregisterGlobalEventListenerAll (
		org.GNOME.Accessibility.EventListener listener) {
		// TODO: implement
	}

	public void deregisterGlobalEventListener (
		org.GNOME.Accessibility.EventListener listener,
		String eventName) {
		// TODO: implement
	}

	public short getDesktopCount () {
		return (short) 1;
	}

	public org.GNOME.Accessibility.Desktop getDesktop (short n) {
		// TODO: more desktops
		return desktops [0];
	}

	public org.GNOME.Accessibility.Desktop[] getDesktopList () {
		return desktops;
	}

	public DeviceEventController getDeviceEventController () {
		if (deviceEventController == null)
			deviceEventController = 
				DeviceEventControllerHelper.narrow 
				(new DeviceEventControllerImpl ().tie ());
		return deviceEventController;
	}

    // From RegistryOperations

	public void unImplemented () {}
	public void unImplemented2 () {}
	public void unImplemented3 () {}
	public void unImplemented4 () {}
	public void unImplemented5 () {}
	public void unImplemented6 () {}

    // From EventListenerOperations
	public void unImplemented_ () {}
	public void unImplemented2_ () {}
	public void unImplemented3_ () {}
	public void unImplemented4_ () {}
}
