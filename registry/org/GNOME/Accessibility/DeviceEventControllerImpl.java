/*
 * java-access-bridge for GNOME 
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Accessibility;

import javax.accessibility.*;
import org.GNOME.Bonobo.*;

public class DeviceEventControllerImpl extends UnknownImpl 
	implements DeviceEventControllerOperations {

	public boolean registerKeystrokeListener (DeviceEventListener listener,
						  KeyDefinition[] keys,
						  int mask,
						  EventType[] type,
						  EventListenerMode mode) {
		return false;
	}

	public void deregisterKeystrokeListener (DeviceEventListener listener,
						 KeyDefinition[] keys,
						 int mask,
						 EventType[] type) {
	}

        public boolean registerDeviceEventListener (DeviceEventListener listener,
						    org.GNOME.Accessibility.EventType[] types) {
	        return false;
        }

        public void deregisterDeviceEventListener (DeviceEventListener listener,
						   org.GNOME.Accessibility.EventType[] eventTypes) {
        }

	public boolean notifyListenersSync (DeviceEvent event) {
		return false;
	}

	public void notifyListenersAsync (DeviceEvent event) {
	}

	public void generateKeyboardEvent (int keycode, String keystring, 
				    KeySynthType type) {
		; // TODO: can this be implemented cleanly?
	}

	public void generateMouseEvent (int x, int y, String eventName) {
		; // TODO: can this be implemented cleanly?
	}

	public void unImplemented () {}
	public void unImplemented2 () {}
	public void unImplemented3 () {}
	public void unImplemented4 () {}
	
}
