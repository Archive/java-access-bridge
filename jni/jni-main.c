/*
 * java-access-bridge for GNOME
 * Copyright 2009 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <jni.h>
#include <stdio.h>
#include <gtk/gtk.h>

JNIEXPORT jint JNICALL JNI_OnLoad (JavaVM *javaVM, void *reserve) {
	g_type_init();
	g_setenv( "NO_AT_BRIDGE", "1", TRUE );
	return JNI_VERSION_1_4;
}

JNIEXPORT void JNICALL JNI_OnUnload (JavaVM *javaVM, void *reserve) {
}

