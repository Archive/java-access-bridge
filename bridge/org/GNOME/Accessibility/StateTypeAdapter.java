/*
 * java-access-bridge for GNOME
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Accessibility;
import javax.accessibility.AccessibleState;

public class StateTypeAdapter extends StateType {

	AccessibleState state;

	public StateTypeAdapter (AccessibleState state) {
		super(valueFromAccessibleState(state));
		this.state = state;
	}

	public String getName() {
		// FIXME: not always the same as at-spi strings
		return state.toDisplayString(java.util.Locale.US);
	}

	static AccessibleState accessibleStateFromStateType (StateType state) {
		AccessibleState astate = null;
		switch (state.value()) {
		case StateType._STATE_ACTIVE:
			astate = AccessibleState.ACTIVE;
			break;
		case StateType._STATE_ARMED:
			astate = AccessibleState.ARMED;
			break;
		case StateType._STATE_BUSY:
			astate = AccessibleState.BUSY;
			break;
		case StateType._STATE_CHECKED:
			astate = AccessibleState.CHECKED;
			break;
		case StateType._STATE_COLLAPSED:
			astate = AccessibleState.COLLAPSED;
			break;
		case StateType._STATE_EDITABLE:
			astate = AccessibleState.EDITABLE;
			break;
		case StateType._STATE_ENABLED:
			astate = AccessibleState.ENABLED;
			break;
		case StateType._STATE_EXPANDABLE:
			astate = AccessibleState.EXPANDABLE;
			break;
        case StateType._STATE_EXPANDED:
		    astate = AccessibleState.EXPANDED;
            break;
		case StateType._STATE_FOCUSABLE:
			astate = AccessibleState.FOCUSABLE;
			break;
		case StateType._STATE_FOCUSED:
			astate = AccessibleState.FOCUSED;
			break;
		case StateType._STATE_HORIZONTAL:
			astate = AccessibleState.HORIZONTAL;
			break;
		case StateType._STATE_ICONIFIED:
			astate = AccessibleState.ICONIFIED;
			break;
		case StateType._STATE_MODAL:
			astate = AccessibleState.MODAL;
			break;
		case StateType._STATE_MULTI_LINE:
			astate = AccessibleState.MULTI_LINE;
			break;
		case StateType._STATE_MULTISELECTABLE:
			astate = AccessibleState.MULTISELECTABLE;
			break;
		case StateType._STATE_OPAQUE:
			astate = AccessibleState.OPAQUE;
			break;
		case StateType._STATE_PRESSED:
			astate = AccessibleState.PRESSED;
			break;
		case StateType._STATE_RESIZABLE:
			astate = AccessibleState.RESIZABLE;
			break;
        case StateType._STATE_SELECTABLE:
            astate = AccessibleState.SELECTABLE;
            break;
		case StateType._STATE_SELECTED:
			astate = AccessibleState.SELECTED;
			break;
		case StateType._STATE_SENSITIVE:
			astate = AccessibleState.ENABLED;
			break;
		case StateType._STATE_SHOWING:
			astate = AccessibleState.SHOWING;
			break;
		case StateType._STATE_SINGLE_LINE:
			astate = AccessibleState.SINGLE_LINE;
			break;
		case StateType._STATE_TRANSIENT:
			astate = AccessibleState.TRANSIENT;
			break;
		case StateType._STATE_VERTICAL:
			astate = AccessibleState.VERTICAL;
			break;
		case StateType._STATE_VISIBLE:
			astate = AccessibleState.VISIBLE;
			break;
		}
		return astate;
	}

	static int valueFromAccessibleState(AccessibleState state) {
		int value = StateType._STATE_INVALID;
		if (state == AccessibleState.ACTIVE) value = StateType._STATE_ACTIVE;
		else if (state == AccessibleState.ARMED) value = StateType._STATE_ARMED;
		else if (state == AccessibleState.BUSY) value = StateType._STATE_BUSY;
		else if (state == AccessibleState.CHECKED) value = StateType._STATE_CHECKED;
		else if (state == AccessibleState.COLLAPSED) 
			value = StateType._STATE_COLLAPSED;// deprecated!
//NYI           else if (state == ) value = StateType._STATE_DEFUNCT;
		else if (state == AccessibleState.EDITABLE) value = StateType._STATE_EDITABLE;
		else if (state == AccessibleState.ENABLED) value = StateType._STATE_ENABLED;
		else if (state == AccessibleState.EXPANDABLE) value = StateType._STATE_EXPANDABLE;
		else if (state == AccessibleState.EXPANDED) value = StateType._STATE_EXPANDED;
		else if (state == AccessibleState.FOCUSABLE) value = StateType._STATE_FOCUSABLE;
		else if (state == AccessibleState.FOCUSED) value = StateType._STATE_FOCUSED;
//NYI		else if (state == AccessibleState.) value = StateType._STATE_HAS_TOOLTIP;
		else if (state == AccessibleState.HORIZONTAL) value = StateType._STATE_HORIZONTAL;
		else if (state == AccessibleState.ICONIFIED) value = StateType._STATE_ICONIFIED;
		else if (state == AccessibleState.MODAL) value = StateType._STATE_MODAL;
		else if (state == AccessibleState.MULTI_LINE) value = StateType._STATE_MULTI_LINE;
		else if (state == AccessibleState.MULTISELECTABLE) value = StateType._STATE_MULTISELECTABLE;
		else if (state == AccessibleState.OPAQUE) value = StateType._STATE_OPAQUE;
		else if (state == AccessibleState.PRESSED) value = StateType._STATE_PRESSED;
		else if (state == AccessibleState.RESIZABLE) value = StateType._STATE_RESIZABLE;
		else if (state == AccessibleState.SELECTABLE) value = StateType._STATE_SELECTABLE;
		else if (state == AccessibleState.SELECTED) value = StateType._STATE_SELECTED;
// NYI          else if (state ==) value = StateType_STATE_SENSITIVE;
		else if (state == AccessibleState.SHOWING) value = StateType._STATE_SHOWING;
		else if (state == AccessibleState.SINGLE_LINE) value = StateType._STATE_SINGLE_LINE;
		else if (state == AccessibleState.TRANSIENT) value = StateType._STATE_TRANSIENT;
		else if (state == AccessibleState.VERTICAL) value = StateType._STATE_VERTICAL;
		else if (state == AccessibleState.VISIBLE) value = StateType._STATE_VISIBLE;
        // Special cases for MANAGES_DESCENDANTS and INDETERMINATE which are 
        // currently not part of JAA 
        // XXX Must fix when they appear in later revision of JAA.
        else if ( state.toDisplayString(java.util.Locale.US) == "managesDescendants" ) value=StateType._STATE_MANAGES_DESCENDANTS;
        else if ( state.toDisplayString(java.util.Locale.US) == "manages descendants" ) value=StateType._STATE_MANAGES_DESCENDANTS;
        else if ( state.toDisplayString() == "indeterminate" ) value=StateType._STATE_INDETERMINATE;
		else value = StateType._STATE_INVALID;
		return value;		
	}
}
