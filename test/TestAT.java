/*
 * java-access-bridge for GNOME
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
import org.GNOME.Bonobo.*;
import org.GNOME.Accessibility.*;
import org.omg.PortableServer.*;
import org.omg.CORBA.*;
import java.io.*;

public class TestAT {
	
	public TestAT () {
		String vm_rev = System.getProperty("java.version");
		if (vm_rev.compareTo("1.4.0") < 0) {
			System.out.println("WARNING: TestAT requires " +
					   "JVM version 1.4.0 or greater.");
		} else {
			runTest ();
		}
	}

	public static Registry getRegistryObject() {
		return AccessUtil.getRegistryObject ();
	}

	private void runTest () {
		int desktopCount = getRegistryObject ().getDesktopCount ();
		Accessible desktop = getRegistryObject().getDesktop((short)0);
		System.out.println (desktopCount + " desktops found.");
		printTree (desktop, "");
		desktop.unref();
	}

	private void printTree (Accessible node, String prefix) {
		prefix += "*";
		System.out.println (prefix + 
				    " [" + node.getRoleName () + "] \"" +
				    node.name () + "\"");
		int n = node.childCount ();
		for (int i = 0; i < n; ++i) {
			Accessible child = node.getChildAtIndex (i);
			printTree (child, prefix);
			child.unref ();
		}
	}

	public static void main(String args[]) {
		new TestAT ();
	}
}
